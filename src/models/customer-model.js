const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Customer = new Schema(
    {
        city:{
            type: String,
            required: true
        },
        district:{
            type: String,
            required: true
        },
        roomNumber:{
            type: String,
            required: true
        },
        email:{
            type: String,
            required: true
        },
        phone:{
            type: String,
            required: true
        },
    },
    { timestamps: true },
)

module.exports = mongoose.model('customers', Customer)