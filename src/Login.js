import React from 'react';
import './App.css';
/*
import './css/animate.css';
import './css/bootstrap.min.css';
import './css/bootstrap-theme.min.css';
import './css/bootstrap-theme.css';
import './css/custom.css';
import './css/versions.css';
import './style.css';
import './css/bootstrap.css';
import './css/main.css';
import './css/slideshow.css';
import './css/style.css';
import './css/util.css';
import './css/responsive.css';
import './css/flaticon.css';
import './css/owl.carousel.css';
*/

const labelStyle = {
  fontsize: "11px",
};

const kariaStyle = {
 color: "#2ccbdd",
};

const kariaButtonStyle = {
    backgroundColor: "#2ccbdd",
   };

const whiteStyle = {
  color: "#ffffff",
};
const greyStyle = {
    color: "#fafeff",
  };

  const darkGreyStyle = {
    color: "#4f4f4f",
  };


const itemStyle = {
    color: "#ffffff",
    
};


class LoginPage extends React.Component  {
    //constructor
    constructor(props) {
        super(props);
        this.state = {
            city:"",
            district:"",
            roomNumber:"",
            email:"",
            countryCode:"",
            phone:"",
        };
    };

    handlechange = async(event) => {
    
    const target = event.target;
    const name = target.name;
    const value = target.value;

    await this.setState({
      [name]: value,
    });  
  };
  
    contact = async (event) => {
    event.preventDefault();
    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
        city: this.state.city,
        district: this.state.district,
        roomNumber: this.state.roomNumber,
        email: this.state.email,
        countryCode: this.state.countryCode,
        phone: this.state.phone
    });
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    
    fetch("http://localhost:3000/api/customer", requestOptions)
      .then(response => response.text())
      .then(result => alert("Kaydınız oluşturuldu. Sizi arayacağız."))
      .catch(error => alert('error', error));

    };


  render(){
    return (
    <div className="App">
       
 

	
	<ul className='slideshow'>
		<li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
		</li>
		
		
	</ul>
	
    <div className="parallax first-section">
        <div className="container">
            <div className="row">
               
				<div className="col-md-12 col-sm-12 center">
                    
                    
                        <h2><span style={kariaStyle}><b>404 </b></span>Not Found!</h2>
                       
                        
                        
                    
                </div>
            </div>
        </div>
    </div>
	
	

    

    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
      </div>
    );
  }
}

export default LoginPage;