import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';


const kariaStyle = {
 color: "#2ccbdd",
};

const kariaButtonStyle = {
    backgroundColor: "#2ccbdd",
   };


class NotFoundPage extends React.Component  {

  render(){
    return (
    <div className="App">
       
 

	
	<ul className='slideshow'>
		<li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
		</li>
		
		
	</ul>
	
    <div className="parallax first-section">
        <div className="container">
            <div className="row">
               
				<div className="col-md-12 col-sm-12 center">
                    
                    
                        <h2><span style={kariaStyle}><b>404 </b></span><span style={{color:"#ffffff"}}>Aradığınız Sayfa Bulunamadı!</span></h2>
                        <br></br>
                        <Link to="/"><button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">Ana Sayfaya Dön</button></Link>
                        
                    
                </div>
            </div>
        </div>
    </div>
	
	

    

    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
      </div>
    );
  }
}

export default NotFoundPage;