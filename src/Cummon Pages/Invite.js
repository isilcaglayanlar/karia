import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

const labelStyle = {
    fontsize: "11px",
  };
  
  const kariaStyle = {
   color: "#2ccbdd",
  };
  
  const kariaButtonStyle = {
      backgroundColor: "#2ccbdd",
      color: "#ffffff",
     };

 

class InvitePage extends React.Component  {
    canBeSubmitted() {
        const { email, password } = this.state;
        return (
          email.length > 0 &&
          password.length > 0
        );
      }
      
      handleSubmit = (evt) => {
        if (!this.canBeSubmitted()) {
          evt.preventDefault();
          return;
        }
        // actual submit logic...
      };
    
    //constructor
    constructor(props) {
        
        super(props);
        
        this.state = {
           
            email:"",
            c1:"",
            c2:"",
            referenceCode:""
        };
    };


    handlechange = async(event) => {
    
    const target = event.target;
    const name = target.name;
    const value = target.value;

    await this.setState({
      [name]: value,
    });  
  };
  
    contact = async (event) => {
    event.preventDefault();
    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://api.kariabnb.com/contacts/by-email/"+this.state.email+"/", requestOptions)
        .then(response => response.text())
        .then((dataStr) => {
            let str = JSON.parse(dataStr);
            if(str.data.verified){
            this.setState({ referenceCode: str.data.referenceCode })
            }
            else{
                alert("Emailinizi onaylayınız.")
            }
            
        })
        .catch(error => console.log('error', error));
    

    };

  
  render(){
      
    return (
        
    <div className="App">
        <header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to= "/">Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
       <div className="all-title-box4">
		      <div className="container">
			      <div className="row">
			      	<div className="col-md-12">
				        
					      
				      </div>
			      </div>
		      </div>
	      </div>
	
	<div id="support" className="section wb2">
        <div className="container">
            <div className="section-title text-center">
                <h3>Tavsiye Et ve Kazan!</h3>
                <p className="lead"><span stlye={kariaStyle}>KariaBNB</span>'yi arkadaşlarına tavsiye et, 350₺ kazan!</p>
            </div>
            <div className="row">
                <div className="col-md-8 col-md-offset-2">
                    <div className="contact_form" >
                        <div id="message"></div>
                        <form className="row" onSubmit={this.contact} method="post">
                            <fieldset className="row-fluid center">
                                
                                <div className="col-lg-12 col-md-6 col-sm-6 col-xs-12 center" >
                                    <input type="email" name="email" id="email1" className="form-control" value={this.state.email} onChange={this.handlechange} required="required" placeholder="Emailiniz"></input>
                                </div>
                                
                                

                                <br></br>
                                <br></br>
                                <div className="col-lg-12 col-md-6  col-xs-12">
                                    <button type="submit" id= "register" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} disabled={!this.state.email || this.state.c1 !== "on" || this.state.c2 !== "on" }  data-target="#centralModalSm">Kod Gönder</button>
                                    <div className="checkbox">
                                        <label style={labelStyle}/><input type="checkbox" id= "c1" name="c1" required="required" defaultChecked={this.state.c1} onChange={this.handlechange}/><a href="/Davet-Sartlari"><u>Kullanım Şartları</u></a>'nı okudum, kabul ediyorum.
                                    </div>
                                    <div className="checkbox">
                                        <label style={labelStyle}>
                                            <input type="checkbox" id="c2" name="c2" required="required" defaultChecked={this.state.c2} onChange={this.handlechange}/>
                                                Üyelik Sözleşmesi, Gizlilik & Güvenlik Politikası Ve Kişisel Verilerin Korunması Kanunu uyarınca 
                                                    <a href="/Aydinlatma-Metni"><u> Aydınlatma Metni</u></a>
                                                'ni okudum, kabul ediyorum.
                                            
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
				
				
            </div>
        </div>
    </div>
    <div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                                  aria-hidden="true">

                                  <div class="modal-dialog modal-l" role="document">


                                    <div class="modal-content">
                                      <div class="modal-header">
                                        
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="false">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <ul>
                                          <li><h3>Tavsiye Kodunuz:</h3></li>
                                          <li><span style={{color:"#2ccbdd",fontSize:"45px"}}>{this.state.referenceCode}</span></li>
                                          
                                          
                                        </ul>
                                        
                                        <button class="btn btn-light btn-radius btn-brd " style={kariaButtonStyle}
                                        
                                        onClick={() =>  navigator.clipboard.writeText(this.state.referenceCode)}
                                        >
                                        Kopyala
                                        </button>
                                        <br></br>
                                        
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Pencereyi Kapat</button>
                                       
                                      </div>
                                    </div>
                                  </div>
                                </div>
    <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default InvitePage;