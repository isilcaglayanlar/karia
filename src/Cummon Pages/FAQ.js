import React from 'react';
import './App.css';
import './css/faq.css';
import { Link } from 'react-router-dom';

class FAQPage extends React.Component  {


  render(){
    return (
    <div className="App">

<header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/#">Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><a className="active" href="/FAQ">SSS</a></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    <div class="all-title-box2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
			
					
				</div>
			</div>
		</div>
	</div>
<div class="container">

<div class="page-header">
    <h1>Sıkça Sorulan Sorular</h1>
</div>

<div class="container">
    <br />
    
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <strong>KariaBNB</strong> ile ilgili sıkça aldığımız soruların cevaplarını bu sayfada bulabilirsiniz. Daha fazla bilgi için lütfen bizimle <a href="/Contact"><u>iletişime geçin</u></a>.
    </div>

    <br />

    <div class="panel-group" id="accordion">
        <div class="faqHeader">Temel Sorular</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">KariaBNB ne yapar?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                Kısa dönemli kiralama çözümleriyle; mülk sahiplerinin, gelirlerini artırmalarına yardımcı 
                olurken aynı zamanda iş yüklerini azaltıyoruz. Airbnb ve diğer kısa vadeli kiralık dairelerin; 
                günlük işlemlerini, çevrimiçi kayıt, fotoğrafçılık, fiyatlandırma yönetimi, misafir iletişimi, 
                temizlik, çamaşır yıkama, banyo malzemeleri ve misafirlerin check-in işlemlerini yönetiyoruz.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">KariaBNB şu anda nerede çalışıyor?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                   <ul>
                   <li><b><Link to="/Services">Profil Yönetimi:</Link></b> Tüm Türkiye</li>
                   <li><b><Link to="/Services">Tam Yönetim:</Link></b> Bodrum </li>
                   </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">İlanımı hangi platformlarda listeliyorsunuz?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                Listelerimiz için aşağıdaki rezervasyon platformlarını kullanıyoruz:
                <ul>
                <li><b><span style={{color:"#ff385c"}} >AirBNB</span></b></li>
                <li><b><span style={{color:"#003580"}} >Booking.com</span></b></li>
                <li><b><span style={{color:"#34e0a1"}} >TripAdvisor</span></b></li>
                <li><b><span style={{color:"#ec2c46"}} >Hotels.com</span></b></li>
                <li><b><span style={{color:"#ee229c"}} >lastminute.com</span></b></li>
                <li>
                    <b><span style={{color:"#0080af"}} >Tri</span></b>
                    <b><span style={{color:"#f48e00"}} >va</span></b>
                    <b><span style={{color:"#c94938"}} >go</span></b>
                </li>
                <li><b><span style={{color:"#003057"}} >hotelbeds</span></b></li>
                <li>
                    <b><span style={{color:"#ffcc00"}} >e</span></b>
                    <b><span style={{color:"#0b3c9d"}} >dreams</span></b>
                </li>
                <li><b><span style={{color:"#d40e15"}} >TUI</span></b></li>
                <li><b><span style={{color:"#ff0000"}} >atravea</span></b></li>
                <li><b><span style={{color:"#0acdd7"}} >misterb&b</span></b></li>
                <li>
                    <b><span style={{color:"#872526"}} >FLIP</span></b>
                    <b><span style={{color:"#cf772a"}} >KEY</span></b>
                </li>
                <li>
                    <b><span style={{color:"#000000"}} >H</span></b>
                    <b><span style={{color:"#1dbfce"}} >O</span></b>
                    <b><span style={{color:"#000000"}} >USETRIP</span></b>
                </li>
                <li><b>ve daha fazlası...</b></li>
                </ul>
                <br></br>
                İlanızın listeleneceği platform; mülkünüzün konumuna, fiyatına ve benzersiz özelliklerine de 
                bağlıdır; yani, mülkünüz, platforma özgü gereksinimlere uygun değilse 
                yukarıdaki platformların tümünde listelenmeyebilir. 
                </div>
            </div>
        </div>
      

        <div class="faqHeader">Güvenlik</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Mülkümün güvenliğini sağlamak için ne tür önlemler alınmaktadır?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                Tüm rezervasyonlarda, ismiyle rezervasyonun ismini eşleştirdiğimiz konuk kimliğini, başka birinin adına yapılan bir rezervasyon için dairede kalan kişinin kimliğini isteriz. Rezervasyonu yapan kişinin kimliğini alamazsak, karşılayan kişi bunu bize bildirecek ve sizi ve mülkünüzü güvende tutmak için rezervasyonu iptal edeceğiz.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Konuklar mülküme zarar verirse ne olur?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                Konukları incelemek ve sizin yerinizde kimlerin kalacağını kontrol etmemizi sağlamak için güçlü süreçlerimiz var, ancak kısa vadeli piyasada mevcut olduğunda mülkte herhangi bir değer bulundurmamanızı rica ediyoruz. Beklenmedik bir durumda, misafirler mülkünüze zarar verir veya malınızı çalarsa, sorun çözme uzmanlarından oluşan ekibimiz, platform ve misafirlerle irtibat kurmanız için uğrayacağınız zararı telafi etmek için ellerinden geleni yapacaktır. Rezervasyon Airbnb aracılığıyla yapıldıysa, ek ücret ödemeden 1.000.000 $ 'lık Ev Sahibi Garantisi kapsamına hak kazanırsınız.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">KariaBNB ile ev sahipliği yapmaya başlamak için hangi sertifikalara ihtiyacım var?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                Pazara bağlı olarak, evinizi bizimle birlikte listelemek için özel belgeler talep edebiliriz. Ancak, mülkünüzün tüm yerel sağlık ve güvenlik düzenlemelerinin yanı sıra hükümetin tüm kural ve düzenlemelerine tam olarak uygun olmasını bekliyoruz. 
                </div>
            </div>
        </div>
        

        <div class="faqHeader">Ücretler ve Ödeme</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Ne zaman ödeme alacağım?</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                Ödemeler, o ay içinde çıkış yapılan rezervasyonlar için her ayın son iş gününe kadar işlenir.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">KariaBNB aracılığıyla kiralık mülkümden nasıl daha fazla kazanabilirim?</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
                Şehir genelinde doluluk oranları, seyahat eğilimleri, mülk konumu, etkinlikler, mevsimsellik, rakip fiyatları ve daha fazlası gibi faktörleri yansıtan kapsamlı bir fiyatlandırma stratejisi geliştirdik. Deneyimli konukseverlik stratejistlerinden oluşan ekibimiz, gelirlerinizin yıl boyunca en üst düzeye çıkmasını sağlayacaktır.
                </div>
            </div>
        </div>
        
      
        
  

      </div>
      </div>
      </div>

  
      <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
    }
    }
  
  
  


export default FAQPage;