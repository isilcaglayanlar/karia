import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';


class InviteTermsPage extends React.Component  {


  render(){
    return (
    <div className="App">
       <header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/#"> Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
 

	
	
	
    <div className="parallax first-section">
        <div className="container">
            <div className="row">
               
				<div className="col-md-12 col-sm-12 center " style={{backgroundColor:"#e8e8e8"}}>
                    <br></br>
                    <br></br>
                    <ul>
                    <li><h1><b><span style={{color:"#000000"}}> KULLANIM ŞARTLARI</span></b></h1></li>
                    <br>
                    </br>
                    
                    <li><b>1.</b> Bir tavsiyenin "Başarılı Yönlendirme" olarak nitelendirilmesi için, yönlendirdiğiniz yeni ev sahibinin KariaBNB ile bir "Tam Yönetim" sözleşmesi imzalaması ve sözleşmenin başlangıç ​​tarihinin ilk yılı içinde en az 8 haftalık kullanılabilirlik sunması gerekir.</li>
                    <li><b>2.</b> Hotels.com vb. web sitelerinde listelenecektir ve tüm konuk iletişimi, reklamları ve işlemleri doğrudan KariaBNB tarafından yönetilecektir.</li>
                    <li><b>3.</b> Uygun Mülk, KariaBNB'nin hizmet verdiği bir bölgede yer alan ve ulaşılabilen ortalama günlük ücret, mülkün durumu ve satılabilirliğini etkileyen bu tür faktörler dahil -ancak bunlarla sınırlı olmamak üzere- KariaBNB'nin uygunluk kriterlerini karşılayan tamamen mobilyalı bir mülk olacaktır. Bir mülkün uygun olup olmadığının değerlendirilmesi tamamen KariaBNB'nin takdirine bağlıdır.</li>
                    <li><b>4.</b> Bir ev sahibi, kendi ana ikamet yerini veya başka bir şekilde sahip olduğu, ortak sahibi olduğu diğer mülkleri veya yakın akrabalarını önerdiği durumda "Tavsiye Ödülü"nden yararlanamaz. Yönlendirmenin geçerliliğini kanıtlamak için daha fazla belge talep edebiliriz.</li>
                    <li><b>5.</b> Bir "Tavsiye Ödülü", mülk başına yalnızca bir kez ve yalnızca yeni mülkü KariaBNB'ye yönlendiren ilk kişiye ödenir.</li>
                    <li><b>6.</b> Başarılı bir yönlendirmenin ödülü, banka havalesi ile yönlendirenin seçtiği banka hesabına aktarılacaktır. İlk chech-in tarihinde; hesabınıza 50 TL aktarılır. 8 haftalık başarılı ev sahipliğinden sonra geri kalan miktar hesabınıza aktarılır.</li>
                    <li><b>7.</b> KariaBNB, bu teklifin şartlarını önceden haber vermeksizin herhangi bir zamanda değiştirme, düzeltme ve iptal etme hakkını saklı tutar.</li>
                    </ul> 
                    <br></br>
                </div>
            </div>
        </div>
    </div>
	
	

    

    <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default InviteTermsPage;