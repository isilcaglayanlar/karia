import React from 'react';
import './App.css';
import OwlCarousel  from 'react-owl-carousel';  
import { Link } from 'react-router-dom';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import './css/owl.carousel.css';
import './css/animate.css';
import './css/bootstrap.min.css';
import './css/bootstrap-theme.min.css';
import './css/bootstrap-theme.css';
import './css/custom.css';
import './css/versions.css';
import './css/main.css';
import './css/slideshow.css';
import './css/util.css';
import './css/responsive.css';
import './css/flaticon.css';

import $ from 'jquery';
window.$ = $;


const labelStyle = {
  fontsize: "11px",
};

const kariaStyle = {
 color: "#2ccbdd",
 
};

const kariaButtonStyle = {
    backgroundColor: "#2ccbdd",
    color: "#ffffff",
   };

const whiteStyle = {
  color: "#ffffff",
};
const greyStyle = {
    color: "#fafeff",
  };

const itemStyle = {
    backgroundColor: "#ffffff",
    borderColor:"#ffffff"
};

var data = [
  {
      "il": "İzmir",
      "plaka": 35,
      "ilceleri": [
        "Aliağa",
        "Bayındır",
        "Bergama",
        "Bornova",
        "Çeşme",
        "Dikili",
        "Foça",
        "Karaburun",
        "Karşıyaka",
        "Kemalpaşa",
        "Kınık",
        "Kiraz",
        "Menemen",
        "Ödemiş",
        "Seferihisar",
        "Selçuk",
        "Tire",
        "Torbalı",
        "Urla",
        "Beydağ",
        "Buca",
        "Konak",
        "Menderes",
        "Balçova",
        "Çiğli",
        "Gaziemir",
        "Narlıdere",
        "Güzelbahçe",
        "Bayraklı",
        "Karabağlar"
      ]
    },
    {
      "il": "İstanbul",
      "plaka": 34,
      "ilceleri": [
        "Adalar",
        "Bakırköy",
        "Beşiktaş",
        "Beykoz",
        "Beyoğlu",
        "Çatalca",
        "Eyüp",
        "Fatih",
        "Gaziosmanpaşa",
        "Kadıköy",
        "Kartal",
        "Sarıyer",
        "Silivri",
        "Şile",
        "Şişli",
        "Üsküdar",
        "Zeytinburnu",
        "Büyükçekmece",
        "Kağıthane",
        "Küçükçekmece",
        "Pendik",
        "Ümraniye",
        "Bayrampaşa",
        "Avcılar",
        "Bağcılar",
        "Bahçelievler",
        "Güngören",
        "Maltepe",
        "Sultanbeyli",
        "Tuzla",
        "Esenler",
        "Arnavutköy",
        "Ataşehir",
        "Başakşehir",
        "Beylikdüzü",
        "Çekmeköy",
        "Esenyurt",
        "Sancaktepe",
        "Sultangazi"
      ]
    },
    {
      "il": "Ankara",
      "plaka": 6,
      "ilceleri": [
        "Altındağ",
        "Ayaş",
        "Bala",
        "Beypazarı",
        "Çamlıdere",
        "Çankaya",
        "Çubuk",
        "Elmadağ",
        "Güdül",
        "Haymana",
        "Kalecik",
        "Kızılcahamam",
        "Nallıhan",
        "Polatlı",
        "Şereflikoçhisar",
        "Yenimahalle",
        "Gölbaşı",
        "Keçiören",
        "Mamak",
        "Sincan",
        "Kazan",
        "Akyurt",
        "Etimesgut",
        "Evren",
        "Pursaklar"
      ]
    },
   
  {
    "il": "Adana",
    "plaka": 1,
    "ilceleri": [
      "Aladağ",
      "Ceyhan",
      "Çukurova",
      "Feke",
      "İmamoğlu",
      "Karaisalı",
      "Karataş",
      "Kozan",
      "Pozantı",
      "Saimbeyli",
      "Sarıçam",
      "Seyhan",
      "Tufanbeyli",
      "Yumurtalık",
      "Yüreğir"
    ]
  },
  {
    "il": "Adıyaman",
    "plaka": 2,
    "ilceleri": [
      "Besni",
      "Çelikhan",
      "Gerger",
      "Gölbaşı",
      "Kahta",
      "Merkez",
      "Samsat",
      "Sincik",
      "Tut"
    ]
  },
  {
    "il": "Afyonkarahisar",
    "plaka": 3,
    "ilceleri": [
      "Başmakçı",
      "Bayat",
      "Bolvadin",
      "Çay",
      "Çobanlar",
      "Dazkırı",
      "Dinar",
      "Emirdağ",
      "Evciler",
      "Hocalar",
      "İhsaniye",
      "İscehisar",
      "Kızılören",
      "Merkez",
      "Sandıklı",
      "Sinanpaşa",
      "Sultandağı",
      "Şuhut"
    ]
  },
  {
    "il": "Ağrı",
    "plaka": 4,
    "ilceleri": [
      "Diyadin",
      "Doğubayazıt",
      "Eleşkirt",
      "Hamur",
      "Merkez",
      "Patnos",
      "Taşlıçay",
      "Tutak"
    ]
  },
  {
    "il": "Amasya",
    "plaka": 5,
    "ilceleri": [
      "Göynücek",
      "Gümüşhacıköy",
      "Hamamözü",
      "Merkez",
      "Merzifon",
      "Suluova",
      "Taşova"
    ]
  },
  
  {
    "il": "Antalya",
    "plaka": 7,
    "ilceleri": [
      "Akseki",
      "Alanya",
      "Elmalı",
      "Finike",
      "Gazipaşa",
      "Gündoğmuş",
      "Kaş",
      "Korkuteli",
      "Kumluca",
      "Manavgat",
      "Serik",
      "Demre",
      "İbradı",
      "Kemer",
      "Aksu",
      "Döşemealtı",
      "Kepez",
      "Konyaaltı",
      "Muratpaşa"
    ]
  },
  {
    "il": "Artvin",
    "plaka": 8,
    "ilceleri": [
      "Ardanuç",
      "Arhavi",
      "Merkez",
      "Borçka",
      "Hopa",
      "Şavşat",
      "Yusufeli",
      "Murgul"
    ]
  },
  {
    "il": "Aydın",
    "plaka": 9,
    "ilceleri": [
      "Merkez",
      "Bozdoğan",
      "Efeler",
      "Çine",
      "Germencik",
      "Karacasu",
      "Koçarlı",
      "Kuşadası",
      "Kuyucak",
      "Nazilli",
      "Söke",
      "Sultanhisar",
      "Yenipazar",
      "Buharkent",
      "İncirliova",
      "Karpuzlu",
      "Köşk",
      "Didim"
    ]
  },
  {
    "il": "Balıkesir",
    "plaka": 10,
    "ilceleri": [
      "Altıeylül",
      "Ayvalık",
      "Merkez",
      "Balya",
      "Bandırma",
      "Bigadiç",
      "Burhaniye",
      "Dursunbey",
      "Edremit",
      "Erdek",
      "Gönen",
      "Havran",
      "İvrindi",
      "Karesi",
      "Kepsut",
      "Manyas",
      "Savaştepe",
      "Sındırgı",
      "Gömeç",
      "Susurluk",
      "Marmara"
    ]
  },
  {
    "il": "Bilecik",
    "plaka": 11,
    "ilceleri": [
      "Merkez",
      "Bozüyük",
      "Gölpazarı",
      "Osmaneli",
      "Pazaryeri",
      "Söğüt",
      "Yenipazar",
      "İnhisar"
    ]
  },
  {
    "il": "Bingöl",
    "plaka": 12,
    "ilceleri": [
      "Merkez",
      "Genç",
      "Karlıova",
      "Kiğı",
      "Solhan",
      "Adaklı",
      "Yayladere",
      "Yedisu"
    ]
  },
  {
    "il": "Bitlis",
    "plaka": 13,
    "ilceleri": [
      "Adilcevaz",
      "Ahlat",
      "Merkez",
      "Hizan",
      "Mutki",
      "Tatvan",
      "Güroymak"
    ]
  },
  {
    "il": "Bolu",
    "plaka": 14,
    "ilceleri": [
      "Merkez",
      "Gerede",
      "Göynük",
      "Kıbrıscık",
      "Mengen",
      "Mudurnu",
      "Seben",
      "Dörtdivan",
      "Yeniçağa"
    ]
  },
  {
    "il": "Burdur",
    "plaka": 15,
    "ilceleri": [
      "Ağlasun",
      "Bucak",
      "Merkez",
      "Gölhisar",
      "Tefenni",
      "Yeşilova",
      "Karamanlı",
      "Kemer",
      "Altınyayla",
      "Çavdır",
      "Çeltikçi"
    ]
  },
  {
    "il": "Bursa",
    "plaka": 16,
    "ilceleri": [
      "Gemlik",
      "İnegöl",
      "İznik",
      "Karacabey",
      "Keles",
      "Mudanya",
      "Mustafakemalpaşa",
      "Orhaneli",
      "Orhangazi",
      "Yenişehir",
      "Büyükorhan",
      "Harmancık",
      "Nilüfer",
      "Osmangazi",
      "Yıldırım",
      "Gürsu",
      "Kestel"
    ]
  },
  {
    "il": "Çanakkale",
    "plaka": 17,
    "ilceleri": [
      "Ayvacık",
      "Bayramiç",
      "Biga",
      "Bozcaada",
      "Çan",
      "Merkez",
      "Eceabat",
      "Ezine",
      "Gelibolu",
      "Gökçeada",
      "Lapseki",
      "Yenice"
    ]
  },
  {
    "il": "Çankırı",
    "plaka": 18,
    "ilceleri": [
      "Merkez",
      "Çerkeş",
      "Eldivan",
      "Ilgaz",
      "Kurşunlu",
      "Orta",
      "Şabanözü",
      "Yapraklı",
      "Atkaracalar",
      "Kızılırmak",
      "Bayramören",
      "Korgun"
    ]
  },
  {
    "il": "Çorum",
    "plaka": 19,
    "ilceleri": [
      "Alaca",
      "Bayat",
      "Merkez",
      "İskilip",
      "Kargı",
      "Mecitözü",
      "Ortaköy",
      "Osmancık",
      "Sungurlu",
      "Boğazkale",
      "Uğurludağ",
      "Dodurga",
      "Laçin",
      "Oğuzlar"
    ]
  },
  {
    "il": "Denizli",
    "plaka": 20,
    "ilceleri": [
      "Acıpayam",
      "Buldan",
      "Çal",
      "Çameli",
      "Çardak",
      "Çivril",
      "Merkez",
      "Merkezefendi",
      "Pamukkale",
      "Güney",
      "Kale",
      "Sarayköy",
      "Tavas",
      "Babadağ",
      "Bekilli",
      "Honaz",
      "Serinhisar",
      "Baklan",
      "Beyağaç",
      "Bozkurt"
    ]
  },
  {
    "il": "Diyarbakır",
    "plaka": 21,
    "ilceleri": [
      "Kocaköy",
      "Çermik",
      "Çınar",
      "Çüngüş",
      "Dicle",
      "Ergani",
      "Hani",
      "Hazro",
      "Kulp",
      "Lice",
      "Silvan",
      "Eğil",
      "Bağlar",
      "Kayapınar",
      "Sur",
      "Yenişehir",
      "Bismil"
    ]
  },
  {
    "il": "Edirne",
    "plaka": 22,
    "ilceleri": [
      "Merkez",
      "Enez",
      "Havsa",
      "İpsala",
      "Keşan",
      "Lalapaşa",
      "Meriç",
      "Uzunköprü",
      "Süloğlu"
    ]
  },
  {
    "il": "Elazığ",
    "plaka": 23,
    "ilceleri": [
      "Ağın",
      "Baskil",
      "Merkez",
      "Karakoçan",
      "Keban",
      "Maden",
      "Palu",
      "Sivrice",
      "Arıcak",
      "Kovancılar",
      "Alacakaya"
    ]
  },
  {
    "il": "Erzincan",
    "plaka": 24,
    "ilceleri": [
      "Çayırlı",
      "Merkez",
      "İliç",
      "Kemah",
      "Kemaliye",
      "Refahiye",
      "Tercan",
      "Üzümlü",
      "Otlukbeli"
    ]
  },
  {
    "il": "Erzurum",
    "plaka": 25,
    "ilceleri": [
      "Aşkale",
      "Çat",
      "Hınıs",
      "Horasan",
      "İspir",
      "Karayazı",
      "Narman",
      "Oltu",
      "Olur",
      "Pasinler",
      "Şenkaya",
      "Tekman",
      "Tortum",
      "Karaçoban",
      "Uzundere",
      "Pazaryolu",
      "Köprüköy",
      "Palandöken",
      "Yakutiye",
      "Aziziye"
    ]
  },
  {
    "il": "Eskişehir",
    "plaka": 26,
    "ilceleri": [
      "Çifteler",
      "Mahmudiye",
      "Mihalıççık",
      "Sarıcakaya",
      "Seyitgazi",
      "Sivrihisar",
      "Alpu",
      "Beylikova",
      "İnönü",
      "Günyüzü",
      "Han",
      "Mihalgazi",
      "Odunpazarı",
      "Tepebaşı"
    ]
  },
  {
    "il": "Gaziantep",
    "plaka": 27,
    "ilceleri": [
      "Araban",
      "İslahiye",
      "Nizip",
      "Oğuzeli",
      "Yavuzeli",
      "Şahinbey",
      "Şehitkamil",
      "Karkamış",
      "Nurdağı"
    ]
  },
  {
    "il": "Giresun",
    "plaka": 28,
    "ilceleri": [
      "Alucra",
      "Bulancak",
      "Dereli",
      "Espiye",
      "Eynesil",
      "Merkez",
      "Görele",
      "Keşap",
      "Şebinkarahisar",
      "Tirebolu",
      "Piraziz",
      "Yağlıdere",
      "Çamoluk",
      "Çanakçı",
      "Doğankent",
      "Güce"
    ]
  },
  {
    "il": "Gümüşhane",
    "plaka": 29,
    "ilceleri": [
      "Merkez",
      "Kelkit",
      "Şiran",
      "Torul",
      "Köse",
      "Kürtün"
    ]
  },
  {
    "il": "Hakkari",
    "plaka": 30,
    "ilceleri": [
      "Çukurca",
      "Merkez",
      "Şemdinli",
      "Yüksekova"
    ]
  },
  {
    "il": "Hatay",
    "plaka": 31,
    "ilceleri": [
      "Altınözü",
      "Arsuz",
      "Defne",
      "Dörtyol",
      "Hassa",
      "Antakya",
      "İskenderun",
      "Kırıkhan",
      "Payas",
      "Reyhanlı",
      "Samandağ",
      "Yayladağı",
      "Erzin",
      "Belen",
      "Kumlu"
    ]
  },
  {
    "il": "Isparta",
    "plaka": 32,
    "ilceleri": [
      "Atabey",
      "Eğirdir",
      "Gelendost",
      "Merkez",
      "Keçiborlu",
      "Senirkent",
      "Sütçüler",
      "Şarkikaraağaç",
      "Uluborlu",
      "Yalvaç",
      "Aksu",
      "Gönen",
      "Yenişarbademli"
    ]
  },
  {
    "il": "Mersin",
    "plaka": 33,
    "ilceleri": [
      "Anamur",
      "Erdemli",
      "Gülnar",
      "Mut",
      "Silifke",
      "Tarsus",
      "Aydıncık",
      "Bozyazı",
      "Çamlıyayla",
      "Akdeniz",
      "Mezitli",
      "Toroslar",
      "Yenişehir"
    ]
  },
  
  {
    "il": "Kars",
    "plaka": 36,
    "ilceleri": [
      "Arpaçay",
      "Digor",
      "Kağızman",
      "Merkez",
      "Sarıkamış",
      "Selim",
      "Susuz",
      "Akyaka"
    ]
  },
  {
    "il": "Kastamonu",
    "plaka": 37,
    "ilceleri": [
      "Abana",
      "Araç",
      "Azdavay",
      "Bozkurt",
      "Cide",
      "Çatalzeytin",
      "Daday",
      "Devrekani",
      "İnebolu",
      "Merkez",
      "Küre",
      "Taşköprü",
      "Tosya",
      "İhsangazi",
      "Pınarbaşı",
      "Şenpazar",
      "Ağlı",
      "Doğanyurt",
      "Hanönü",
      "Seydiler"
    ]
  },
  {
    "il": "Kayseri",
    "plaka": 38,
    "ilceleri": [
      "Bünyan",
      "Develi",
      "Felahiye",
      "İncesu",
      "Pınarbaşı",
      "Sarıoğlan",
      "Sarız",
      "Tomarza",
      "Yahyalı",
      "Yeşilhisar",
      "Akkışla",
      "Talas",
      "Kocasinan",
      "Melikgazi",
      "Hacılar",
      "Özvatan"
    ]
  },
  {
    "il": "Kırklareli",
    "plaka": 39,
    "ilceleri": [
      "Babaeski",
      "Demirköy",
      "Merkez",
      "Kofçaz",
      "Lüleburgaz",
      "Pehlivanköy",
      "Pınarhisar",
      "Vize"
    ]
  },
  {
    "il": "Kırşehir",
    "plaka": 40,
    "ilceleri": [
      "Çiçekdağı",
      "Kaman",
      "Merkez",
      "Mucur",
      "Akpınar",
      "Akçakent",
      "Boztepe"
    ]
  },
  {
    "il": "Kocaeli",
    "plaka": 41,
    "ilceleri": [
      "Gebze",
      "Gölcük",
      "Kandıra",
      "Karamürsel",
      "Körfez",
      "Derince",
      "Başiskele",
      "Çayırova",
      "Darıca",
      "Dilovası",
      "İzmit",
      "Kartepe"
    ]
  },
  {
    "il": "Konya",
    "plaka": 42,
    "ilceleri": [
      "Akşehir",
      "Beyşehir",
      "Bozkır",
      "Cihanbeyli",
      "Çumra",
      "Doğanhisar",
      "Ereğli",
      "Hadim",
      "Ilgın",
      "Kadınhanı",
      "Karapınar",
      "Kulu",
      "Sarayönü",
      "Seydişehir",
      "Yunak",
      "Akören",
      "Altınekin",
      "Derebucak",
      "Hüyük",
      "Karatay",
      "Meram",
      "Selçuklu",
      "Taşkent",
      "Ahırlı",
      "Çeltik",
      "Derbent",
      "Emirgazi",
      "Güneysınır",
      "Halkapınar",
      "Tuzlukçu",
      "Yalıhüyük"
    ]
  },
  {
    "il": "Kütahya",
    "plaka": 43,
    "ilceleri": [
      "Altıntaş",
      "Domaniç",
      "Emet",
      "Gediz",
      "Merkez",
      "Simav",
      "Tavşanlı",
      "Aslanapa",
      "Dumlupınar",
      "Hisarcık",
      "Şaphane",
      "Çavdarhisar",
      "Pazarlar"
    ]
  },
  {
    "il": "Malatya",
    "plaka": 44,
    "ilceleri": [
      "Akçadağ",
      "Arapgir",
      "Arguvan",
      "Darende",
      "Doğanşehir",
      "Hekimhan",
      "Merkez",
      "Pütürge",
      "Yeşilyurt",
      "Battalgazi",
      "Doğanyol",
      "Kale",
      "Kuluncak",
      "Yazıhan"
    ]
  },
  {
    "il": "Manisa",
    "plaka": 45,
    "ilceleri": [
      "Akhisar",
      "Alaşehir",
      "Demirci",
      "Gördes",
      "Kırkağaç",
      "Kula",
      "Merkez",
      "Salihli",
      "Sarıgöl",
      "Saruhanlı",
      "Selendi",
      "Soma",
      "Şehzadeler",
      "Yunusemre",
      "Turgutlu",
      "Ahmetli",
      "Gölmarmara",
      "Köprübaşı"
    ]
  },
  {
    "il": "Kahramanmaraş",
    "plaka": 46,
    "ilceleri": [
      "Afşin",
      "Andırın",
      "Dulkadiroğlu",
      "Onikişubat",
      "Elbistan",
      "Göksun",
      "Merkez",
      "Pazarcık",
      "Türkoğlu",
      "Çağlayancerit",
      "Ekinözü",
      "Nurhak"
    ]
  },
  {
    "il": "Mardin",
    "plaka": 47,
    "ilceleri": [
      "Derik",
      "Kızıltepe",
      "Artuklu",
      "Merkez",
      "Mazıdağı",
      "Midyat",
      "Nusaybin",
      "Ömerli",
      "Savur",
      "Dargeçit",
      "Yeşilli"
    ]
  },
  {
    "il": "Muğla",
    "plaka": 48,
    "ilceleri": [
      "Bodrum",
      "Datça",
      "Fethiye",
      "Köyceğiz",
      "Marmaris",
      "Menteşe",
      "Milas",
      "Ula",
      "Yatağan",
      "Dalaman",
      "Seydikemer",
      "Ortaca",
      "Kavaklıdere"
    ]
  },
  {
    "il": "Muş",
    "plaka": 49,
    "ilceleri": [
      "Bulanık",
      "Malazgirt",
      "Merkez",
      "Varto",
      "Hasköy",
      "Korkut"
    ]
  },
  {
    "il": "Nevşehir",
    "plaka": 50,
    "ilceleri": [
      "Avanos",
      "Derinkuyu",
      "Gülşehir",
      "Hacıbektaş",
      "Kozaklı",
      "Merkez",
      "Ürgüp",
      "Acıgöl"
    ]
  },
  {
    "il": "Niğde",
    "plaka": 51,
    "ilceleri": [
      "Bor",
      "Çamardı",
      "Merkez",
      "Ulukışla",
      "Altunhisar",
      "Çiftlik"
    ]
  },
  {
    "il": "Ordu",
    "plaka": 52,
    "ilceleri": [
      "Akkuş",
      "Altınordu",
      "Aybastı",
      "Fatsa",
      "Gölköy",
      "Korgan",
      "Kumru",
      "Mesudiye",
      "Perşembe",
      "Ulubey",
      "Ünye",
      "Gülyalı",
      "Gürgentepe",
      "Çamaş",
      "Çatalpınar",
      "Çaybaşı",
      "İkizce",
      "Kabadüz",
      "Kabataş"
    ]
  },
  {
    "il": "Rize",
    "plaka": 53,
    "ilceleri": [
      "Ardeşen",
      "Çamlıhemşin",
      "Çayeli",
      "Fındıklı",
      "İkizdere",
      "Kalkandere",
      "Pazar",
      "Merkez",
      "Güneysu",
      "Derepazarı",
      "Hemşin",
      "İyidere"
    ]
  },
  {
    "il": "Sakarya",
    "plaka": 54,
    "ilceleri": [
      "Akyazı",
      "Geyve",
      "Hendek",
      "Karasu",
      "Kaynarca",
      "Sapanca",
      "Kocaali",
      "Pamukova",
      "Taraklı",
      "Ferizli",
      "Karapürçek",
      "Söğütlü",
      "Adapazarı",
      "Arifiye",
      "Erenler",
      "Serdivan"
    ]
  },
  {
    "il": "Samsun",
    "plaka": 55,
    "ilceleri": [
      "Alaçam",
      "Bafra",
      "Çarşamba",
      "Havza",
      "Kavak",
      "Ladik",
      "Terme",
      "Vezirköprü",
      "Asarcık",
      "Ondokuzmayıs",
      "Salıpazarı",
      "Tekkeköy",
      "Ayvacık",
      "Yakakent",
      "Atakum",
      "Canik",
      "İlkadım"
    ]
  },
  {
    "il": "Siirt",
    "plaka": 56,
    "ilceleri": [
      "Baykan",
      "Eruh",
      "Kurtalan",
      "Pervari",
      "Merkez",
      "Şirvan",
      "Tillo"
    ]
  },
  {
    "il": "Sinop",
    "plaka": 57,
    "ilceleri": [
      "Ayancık",
      "Boyabat",
      "Durağan",
      "Erfelek",
      "Gerze",
      "Merkez",
      "Türkeli",
      "Dikmen",
      "Saraydüzü"
    ]
  },
  {
    "il": "Sivas",
    "plaka": 58,
    "ilceleri": [
      "Divriği",
      "Gemerek",
      "Gürün",
      "Hafik",
      "İmranlı",
      "Kangal",
      "Koyulhisar",
      "Merkez",
      "Suşehri",
      "Şarkışla",
      "Yıldızeli",
      "Zara",
      "Akıncılar",
      "Altınyayla",
      "Doğanşar",
      "Gölova",
      "Ulaş"
    ]
  },
  {
    "il": "Tekirdağ",
    "plaka": 59,
    "ilceleri": [
      "Çerkezköy",
      "Çorlu",
      "Ergene",
      "Hayrabolu",
      "Malkara",
      "Muratlı",
      "Saray",
      "Süleymanpaşa",
      "Kapaklı",
      "Şarköy",
      "Marmaraereğlisi"
    ]
  },
  {
    "il": "Tokat",
    "plaka": 60,
    "ilceleri": [
      "Almus",
      "Artova",
      "Erbaa",
      "Niksar",
      "Reşadiye",
      "Merkez",
      "Turhal",
      "Zile",
      "Pazar",
      "Yeşilyurt",
      "Başçiftlik",
      "Sulusaray"
    ]
  },
  {
    "il": "Trabzon",
    "plaka": 61,
    "ilceleri": [
      "Akçaabat",
      "Araklı",
      "Arsin",
      "Çaykara",
      "Maçka",
      "Of",
      "Ortahisar",
      "Sürmene",
      "Tonya",
      "Vakfıkebir",
      "Yomra",
      "Beşikdüzü",
      "Şalpazarı",
      "Çarşıbaşı",
      "Dernekpazarı",
      "Düzköy",
      "Hayrat",
      "Köprübaşı"
    ]
  },
  {
    "il": "Tunceli",
    "plaka": 62,
    "ilceleri": [
      "Çemişgezek",
      "Hozat",
      "Mazgirt",
      "Nazımiye",
      "Ovacık",
      "Pertek",
      "Pülümür",
      "Merkez"
    ]
  },
  {
    "il": "Şanlıurfa",
    "plaka": 63,
    "ilceleri": [
      "Akçakale",
      "Birecik",
      "Bozova",
      "Ceylanpınar",
      "Eyyübiye",
      "Halfeti",
      "Haliliye",
      "Hilvan",
      "Karaköprü",
      "Siverek",
      "Suruç",
      "Viranşehir",
      "Harran"
    ]
  },
  {
    "il": "Uşak",
    "plaka": 64,
    "ilceleri": [
      "Banaz",
      "Eşme",
      "Karahallı",
      "Sivaslı",
      "Ulubey",
      "Merkez"
    ]
  },
  {
    "il": "Van",
    "plaka": 65,
    "ilceleri": [
      "Başkale",
      "Çatak",
      "Erciş",
      "Gevaş",
      "Gürpınar",
      "İpekyolu",
      "Muradiye",
      "Özalp",
      "Tuşba",
      "Bahçesaray",
      "Çaldıran",
      "Edremit",
      "Saray"
    ]
  },
  {
    "il": "Yozgat",
    "plaka": 66,
    "ilceleri": [
      "Akdağmadeni",
      "Boğazlıyan",
      "Çayıralan",
      "Çekerek",
      "Sarıkaya",
      "Sorgun",
      "Şefaatli",
      "Yerköy",
      "Merkez",
      "Aydıncık",
      "Çandır",
      "Kadışehri",
      "Saraykent",
      "Yenifakılı"
    ]
  },
  {
    "il": "Zonguldak",
    "plaka": 67,
    "ilceleri": [
      "Çaycuma",
      "Devrek",
      "Ereğli",
      "Merkez",
      "Alaplı",
      "Gökçebey"
    ]
  },
  {
    "il": "Aksaray",
    "plaka": 68,
    "ilceleri": [
      "Ağaçören",
      "Eskil",
      "Gülağaç",
      "Güzelyurt",
      "Merkez",
      "Ortaköy",
      "Sarıyahşi"
    ]
  },
  {
    "il": "Bayburt",
    "plaka": 69,
    "ilceleri": [
      "Merkez",
      "Aydıntepe",
      "Demirözü"
    ]
  },
  {
    "il": "Karaman",
    "plaka": 70,
    "ilceleri": [
      "Ermenek",
      "Merkez",
      "Ayrancı",
      "Kazımkarabekir",
      "Başyayla",
      "Sarıveliler"
    ]
  },
  {
    "il": "Kırıkkale",
    "plaka": 71,
    "ilceleri": [
      "Delice",
      "Keskin",
      "Merkez",
      "Sulakyurt",
      "Bahşili",
      "Balışeyh",
      "Çelebi",
      "Karakeçili",
      "Yahşihan"
    ]
  },
  {
    "il": "Batman",
    "plaka": 72,
    "ilceleri": [
      "Merkez",
      "Beşiri",
      "Gercüş",
      "Kozluk",
      "Sason",
      "Hasankeyf"
    ]
  },
  {
    "il": "Şırnak",
    "plaka": 73,
    "ilceleri": [
      "Beytüşşebap",
      "Cizre",
      "İdil",
      "Silopi",
      "Merkez",
      "Uludere",
      "Güçlükonak"
    ]
  },
  {
    "il": "Bartın",
    "plaka": 74,
    "ilceleri": [
      "Merkez",
      "Kurucaşile",
      "Ulus",
      "Amasra"
    ]
  },
  {
    "il": "Ardahan",
    "plaka": 75,
    "ilceleri": [
      "Merkez",
      "Çıldır",
      "Göle",
      "Hanak",
      "Posof",
      "Damal"
    ]
  },
  {
    "il": "Iğdır",
    "plaka": 76,
    "ilceleri": [
      "Aralık",
      "Merkez",
      "Tuzluca",
      "Karakoyunlu"
    ]
  },
  {
    "il": "Yalova",
    "plaka": 77,
    "ilceleri": [
      "Merkez",
      "Altınova",
      "Armutlu",
      "Çınarcık",
      "Çiftlikköy",
      "Termal"
    ]
  },
  {
    "il": "Karabük",
    "plaka": 78,
    "ilceleri": [
      "Eflani",
      "Eskipazar",
      "Merkez",
      "Ovacık",
      "Safranbolu",
      "Yenice"
    ]
  },
  {
    "il": "Kilis",
    "plaka": 79,
    "ilceleri": [
      "Merkez",
      "Elbeyli",
      "Musabeyli",
      "Polateli"
    ]
  },
  {
    "il": "Osmaniye",
    "plaka": 80,
    "ilceleri": [
      "Bahçe",
      "Kadirli",
      "Merkez",
      "Düziçi",
      "Hasanbeyli",
      "Sumbas",
      "Toprakkale"
    ]
  },
  {
    "il": "Düzce",
    "plaka": 81,
    "ilceleri": [
      "Akçakoca",
      "Merkez",
      "Yığılca",
      "Cumayeri",
      "Gölyaka",
      "Çilimli",
      "Gümüşova",
      "Kaynaşlı"
    ]
  }
]


function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].plaka == nameKey) {
            return myArray[i];
        }
    }
}
$( document ).ready(function() {
  $.each(data, function( index, value ) {
    $('#Iller').append($('<option>', {
       
        value: value.il,
        val:  value.plaka,
        text: value.il
    }));
  
  });
  $("#Iller").change(function(){
    var valueSelected = this.val;
    if($('#Iller').val() > 0) {
      $('#Ilceler').html('');
      $('#Ilceler').append($('<option>', {
        value: 0,
        text:  'İlçe Seçiniz'
      }));
      $('#Ilceler').prop("disabled", false);
      var resultObject = search($('#Iller').val(), data);
      $.each(resultObject.ilceleri, function( index, value ) {
        $('#Ilceler').append($('<option>', {
            value: value,
            text:  value
        }));
      });
      return false;
    }
    $('#Ilceler').prop("disabled", true);
    
  });
});

  
class MainPage extends React.Component  {

 
    //constructor
    
    constructor(props) {
        super(props);
        this.state = {
            city:"",
            district:"",
            roomNumber:"",
            email:"",
            countryCode:"",
            phone:"",

            options: {
              loop: true,
              margin: 30,
              nav:true,
              
              responsive:{
                  0: {
                      items: 1,
                  },
                  600: {
                      items: 1,
                  },
                  1000: {
                      items: 3,
                  },
              },
          },
        };
    };

    handlechange = async(event) => {
    
    const target = event.target;
    const name = target.name;
    const value = target.value;

    await this.setState({
      [name]: value,
    });  
  };
  
    contact = async (event) => {
    event.preventDefault();
    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
      
    

    var raw = JSON.stringify({
        city: "city",
        district: this.state.district,
        roomNumber: this.state.roomNumber,
        email: this.state.email,
        countryCode: "+90",
        phone: this.state.phone,
        

    });
    console.log(raw)
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    


    fetch("http://api.kariabnb.com/customers/", requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => alert('error', error));

    };
   
   
  render(){
    return (
    <div className="App">
       
   
      
    <header className="header header_style_01" >
            <nav className="megamenu navbar navbar-default" >
                <div className="container-fluid" style={{backgroundColor:"#ffffff", boxShadow:"none"}} >
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><a className="active" href="/">Ana Sayfa</a></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

	
	<ul className='slideshow'>
		<li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
        </li>
        <li>
			<span>Summer</span>
		</li>
		
		
	</ul>
	
    <div className="parallax first-section">
        <div className="container">
            <div className="row">
            <div className="col-md-6 col-xs-3 col-ls-3 wow slideInLeft clearfix">
                    
                    <div className="value">
                                  <div className="value-text1">
                                    <span style={{color:"#2ccbdd"}}><b>KariaBNB</b></span> İle Evinizin Değerine Değer Katın!
                                  </div>
                                  <br></br>
                                  <div className="value-text2">
                                    Evinizi kısa/orta dönem kiraya vererek daha yüksek getiri elde edebilirsiniz.
                                  </div>
                                </div>
                            </div>
                <div className="col-md-6 col-xs-6 col-ls-12 wow slideInLeft clearfix">
                
                    <div className="contact_form">
                        <br></br>
                        
                        <h3><i className="fa fa-money global-radius" style={kariaStyle}></i> Gelirinizi Hesaplayın</h3>
                        
                        <form  className="row" onSubmit={this.contact} method="post">
                            <fieldset className="row-fluid">
                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label className="sr-only" required="required">İl Seçiniz</label>
                                    <select name="city" onChange={this.handlechange} className="form-control" data-style="btn-white" id="Iller">
                                        <option value="0">İl</option>
                                    </select>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label className="sr-only" required="required">İlçe Seçiniz</label>
                                    <select name="district" onChange={this.handlechange}  className= "form-control"  data-style="btn-white" id="Ilceler" disabled="disabled">
                                        <option value="0">İlçe</option>
                                    </select>
                                </div>
                                
                                <div className="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <label className="sr-only">Oda Sayısı</label>
                                    <select name="roomNumber"  onChange={this.handlechange} id="oda" className="room form-control" required="required" data-style="btn-white">
                                        <option value="odasec">Oda Sayısı</option>
                                        <option value="1">Stüdyo(1+0)</option>
                                        <option value="2">1+1</option>
                                        <option value="3">1.5+1</option>
                                        <option value="4">2+0</option>
                                        <option value="5">2+1</option>
                                        <option value="6">2.5+1</option>
                                        <option value="7">2+2</option>
                                        <option value="8">3+1</option>
                                        <option value="9">3.5+1</option>
                                        <option value="10">3+2</option>
                                        <option value="11">4+1</option>
                                        <option value="12">4.5+1</option>
                                        <option value="13">4+2</option>
                                        <option value="14">4+3</option>
                                        <option value="15">4+4</option>
                                        <option value="16">5+1</option>
                                        <option value="17">5+2</option>
                                        <option value="18">5+3</option>
                                        <option value="19">5+4</option>
                                        <option value="20">6+1</option>
                                        <option value="21">6+2</option>
                                        <option value="22">6+3</option>
                                        <option value="23">7+1</option>
                                        <option value="24">7+2</option>
                                        <option value="25">7+3</option>
                                        <option value="26">8+1</option>
                                        <option value="27">8+2</option>
                                        <option value="28">8+3</option>
                                        <option value="29">8+4</option>
                                        <option value="30">7+2</option>
                                        <option value="31">7+3</option>
                                        <option value="32">8+1</option>
                                        <option value="33">8+2</option>
                                        <option value="34">8+3</option>
                                        <option value="35">8+4</option>
                                        <option value="36">9+1</option>
                                        <option value="37">9+2</option>
                                        <option value="38">9+3</option>
                                        <option value="39">9+4</option>
                                        <option value="40">9+5</option>
                                        <option value="41">9+6</option>
                                        <option value="42">10+1</option>
                                        <option value="43">10+2</option>
                                        <option value="44">10 Üzeri</option>
                                    </select>
                                </div>
                                
                                <div className="col-lg-12 col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" value={this.state.email} onChange={this.handlechange} id="email1" className="form-control" required="required" placeholder="Email"></input>
                                </div>
                                
                                <div className="col-lg-3 col-md-6 col-sm-6 col-xs-12">           
                                            <select name="countryCode"  onChange={this.handlechange} className="form-control" id="">
                                            <option value="90" >TR (+90)</option>
                                            <option value="44" >UK (+44)</option>
                                            <option value="1">USA (+1)</option>
                                            <option value="213">Algeria (+213)</option>
                                            <option value="376">Andorra (+376)</option>
                                            <option value="1264">Anguilla (+1264)</option>
                                            <option value="1268">Antigua &amp; Barbuda (+1268)</option>
                                            <option value="54">Argentina (+54)</option>
                                            <option value="374">Armenia (+374)</option>
                                            <option value="297">Aruba (+297)</option>
                                            <option value="61">Australia (+61)</option>
                                            <option value="43">Austria (+43)</option>
                                            <option value="994">Azerbaijan (+994)</option>
                                            <option value="1242">Bahamas (+1242)</option>
                                            <option value="973">Bahrain (+973)</option>
                                            <option  value="880">Bangladesh (+880)</option>
                                            <option  value="1246">Barbados (+1246)</option>
                                            <option  value="375">Belarus (+375)</option>
                                            <option  value="32">Belgium (+32)</option>
                                            <option  value="501">Belize (+501)</option> 
                                            <option value="7">Turkmenistan (+7)</option>
                                            <option  value="993">Turkmenistan (+993)</option>
                                            <option  value="1649">Turks &amp; Caicos Islands (+1649)</option>
                                            <option value="688">Tuvalu (+688)</option>
                                            <option value="256">Uganda (+256)</option>
                                            <option  value="44">UK (+44)</option> 
                                            <option value="380">Ukraine (+380)</option>
                                            <option  value="971">United Arab Emirates (+971)</option>
                                            <option  value="598">Uruguay (+598)</option>
                                            <option value="1">USA (+1)</option>
                                      
                                       
                                    </select>        
                                </div>          

                                <div className="col-lg-9 col-md-6 col-sm-6 col-xs-12">        
                                    <input className="form-control" name="phone" value={this.state.phone} onChange={this.handlechange} type="tel" placeholder="XXX-XXX-XXXX" id="example-tel-input"></input>
                                </div>
                                <div className="col-lg-12 col-md-6  col-xs-12">
                                <button type="submit" className="btn btn-light btn-radius btn-brd " style={kariaButtonStyle} data-toggle="modal"   data-target="#centralModalSm">Hesapla</button>
                                
                                    <div className="checkbox">
                                        <label style={labelStyle}/><input type="checkbox" required="required" value=""/><a href="terms.html"><u>Kullanım şartlarını</u></a> okudum, kabul ediyorum.
                                    </div>
                                    <div className="checkbox">
                                        <label style={labelStyle}>
                                            <input type="checkbox" required="required" value=""></input>
                                                Üyelik Sözleşmesi, Gizlilik & Güvenlik Politikası Ve Kişisel Verilerin Korunması Kanunu uyarınca  
                                                <span><a href="/Aydinlatma-Metni"> <u>Aydınlatma Metni</u></a></span>
                                                'ni okudum, kabul ediyorum.
                                         
                                        </label>
                                    </div>
                                </div>
                                
                            </fieldset>
                        </form>
                       
                    </div>
                </div>
                
                <div className="modal fade" id="centralModalSm" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
                                  aria-hidden="true">

                                  <div className="modal-dialog modal-l" role="document">


                                    <div className="modal-content">
                                      <div className="modal-header">
                                        
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="false">&times;</span>
                                        </button>
                                      </div>
                                      <div className="modal-body">
                                        <ul>
                                          <li>Evinizin potansiyel aylık kazancını hesaplayabilmek için e-posta kutunuza gelen onay e-postasında belirtilen adımları tamamlayınız.</li>
                                          <br></br>
                                          <li>E-posta almadınız mı?</li>
                                          <li>Önemsiz posta klasörünüzü de kontrol edin veya yeni bir e-posta isteyin.</li>
                                          <br></br>
                                          <button type="submit" className="btn btn-light btn-radius btn-brd " style={kariaButtonStyle} data-toggle="modal"   data-target="#centralModal">E-postayı Yeniden Gönder</button>
                                        </ul>
                                       
                                      </div>
                                     
                                    </div>
                                  </div>
                                </div>
                                <div className="modal fade" id="centralModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
                                  aria-hidden="true">

                                  <div className="modal-dialog modal-l" role="document">


                                    <div className="modal-content">
                                      <div className="modal-header">
                                        
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="false">&times;</span>
                                        </button>
                                      </div>
                                      <div className="modal-body">
                                        <ul>
                                          <li>Onay maili tekrar gönderildi.</li>
                                          <br></br>
                                          <li>Evinizin potansiyel aylık kazancını hesaplayabilmek için mailinize gelen onay mailini onaylayınız.</li>

                                          
                                        </ul>
                                       
                                      </div>
                                     
                                    </div>
                                  </div>
                                </div>
			
			
            </div>
        </div>
    </div>
	
	<div className="about-box" > 
		<div className="container" style={kariaButtonStyle}>
			<div className="row" style={kariaButtonStyle}>

				<div className="top-feature owl-carousel" style={kariaButtonStyle}>
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-02.png" className="img-responsive" alt=""/></div>
							<h4><a href="/#">Artan Kira Geliri</a></h4>
							<p>Mauris eu porta orci. In at erat enim. Suspendisse felis erat, volutpat at nisl sit amet, maximus molestie nisi. </p>
              <button type="submit" className="btn btn-light btn-radius btn-brd " style={kariaButtonStyle} data-toggle="modal"   data-target="#centralModalSm">Gelirinizi Hesaplayın</button>
                        </div> 
					</div>
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-01.png" className="img-responsive" alt=""/></div>
							<h4><a href="/#">Konsiyerj Hizmeti</a></h4>
							<p>Mauris eu porta orci. In at erat enim. Suspendisse felis erat, volutpat at nisl sit amet, maximus molestie nisi.</p>
                            <button id="myBtn" type="submit"   className="btn btn-light btn-radius btn-brd grd1 btn-block" >
                                Hizmetlerimiz </button>
                        </div> 
					</div>
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-03.png" className="img-responsive" alt=""/></div>
							<h4><a href="/#">Kişiselleştirilmiş Kiralama Hizmetleri</a></h4>
                            <p>Mauris eu porta orci. In at erat enim. Suspendisse felis erat, </p>
                            <button id="myBtn3" type="submit"   className="btn btn-light btn-radius btn-brd grd1 btn-block" >
                               Teklif Al </button>
						</div> 
					</div>
					
				</div>
			</div>
	
			<div id="testimonials" className=" section bb">
                <div className="col-md-12 section-title row text-center">
                    <div className="col-md-8 col-md-offset-2">
                        <h1><span style={whiteStyle}><b>Evimi neden KariaBNB ile kiralamalıyım?</b></span></h1>
                        <p className="lead"><span style={greyStyle}>Evinizi kısa/orta dönem kiraya vererek daha yüksek getiri elde edebilirsiniz.</span></p>
                        <br></br>
                        <br></br>
                        <OwlCarousel
                    
                    className="owl-responsive"
                   
                    {...this.state.options}
                    nav>
                    
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/getiri.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Yüksek Getiri</span></h3>
                                    <p className="lead">Uzun dönem kiralamaya göre evinizden 2 kat daha fazla getiri elde edin.</p>
                                    <br></br>
                                    <button type="submit" className="btn btn-light btn-radius btn-brd " style={kariaButtonStyle} data-toggle="modal"   data-target="#centralModalSm">Hesapla</button>
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/doluluk.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Yüksek Doluluk</span></h3>
                                    <p className="lead">Dinamik fiyatlama ile sizin için %85’i aşan doluluk oranları sağlıyoruz.</p>
                                    <br></br>
                                    <Link to="/Contact"><button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">İletişime Geç</button></Link>
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/stres.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Stressiz Yönetim</span></h3>
                                    <p className="lead">Misafirlerimizden gelen tüm talepler uzmanlarımız tarafından yönetilir.</p>
                                    <br></br>
                                    <Link to="/Services"><button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">Hizmetlerimiz</button></Link>
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/getiri.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Yüksek Getiri</span></h3>
                                    <p className="lead">Uzun dönem kiralamaya göre evinizden 2 kat daha fazla getiri elde edin.</p>
                                    <br></br>
                                    <button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">Hesapla</button>
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/doluluk.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Yüksek Doluluk</span></h3>
                                    <p className="lead">Dinamik fiyatlama ile sizin için %85’i aşan doluluk oranları sağlıyoruz.</p>
                                    <br></br>
                                    <Link to="/Contact"><button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">İletişime Geç</button></Link>
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/stres.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Stressiz Yönetim</span></h3>
                                    <p className="lead">Misafirlerimizden gelen tüm talepler uzmanlarımız tarafından yönetilir.</p>
                                    <br></br>
                                    <Link to="/Services"><button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">Hizmetlerimiz</button></Link>
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        </OwlCarousel>
                    </div>
                </div>
			</div>
	
		</div>
	</div>
    <div id="testimonials" className="section lb">
        <div className="container">
            <div className="section-title row text-center">
                <div className="col-md-8 col-md-offset-2">
                    <h3><span style={kariaStyle}>KariaBNB</span> Hizmetleri</h3>
                    <p className="lead">Servislerimiz; konuklarınıza, en iyi konaklama deneyimini sağlamak için tasarlandı!</p>
                      <br></br>
                        <OwlCarousel
                    
                    className="owl-responsive"
                   
                    {...this.state.options}
                    nav>
                    
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/testi_01.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Profil ve İlan Yönetimi</span></h3>
                                    <p className="lead">Evinizin ilanlarını sizin için 15+ platformda yayınlıyoruz.</p>
                                    <br></br>
                                   
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/testi_02.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Profesyonel Fotoğraf</span></h3>
                                    <p className="lead">Profesyonel fotoğrafçılarımızla, evinizin fotoğraflarını çekiyoruz.</p>
                                    <br></br>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/testi_03.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Temizlik ve Linen Değişimi</span></h3>
                                    <p className="lead">Evinizi 5 yıldızlı otel konforuna konaklanacak hale getiriyoruz.</p>
                                    <br></br>
                                   
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/testi_04.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Misafir İlişkileri</span></h3>
                                    <p className="lead">Misafirlerimiz için 7/24 iletişim desteği sağlıyoruz.</p>
                                    <br></br>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/testi_05.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Fiyat Optimizasyonu</span></h3>
                                    <p className="lead">Kazancınızı maksimize etmek için dinamik fiyatlandırma yapıyoruz.</p>
                                    <br></br>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div className="item">
                            <div className="testimonial clearfix" >
                                <div className="desc" style={itemStyle} >
                                    <img src="uploads/testi_06.png" alt="" className="img-responsive"></img>
                                    <br></br>
                                    <h3><span style={kariaStyle}>Bakım ve Onarım</span></h3>
                                    <p className="lead">Evde oluşabilecek her problemi hızlı bir şekilde çözüyoruz.</p>
                                    <br></br>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        </OwlCarousel>
                    </div>
                        
                    </div>
                </div>
            </div>
        

    <div id="agent" className="parallax section bb parallax-off">
        <div className="container">
            <div className="section-title row text-center">
            
                <div className="col-md-8 col-md-offset-2 row-md-2">
                    
                    <h3><span style={whiteStyle}>Tavsiye Et, Kazan!</span></h3>
                    <p className="lead"><span style={whiteStyle}>KariaBNB'yi bir arkadaşınıza tavsiye edin, 350₺ kazanın! </span></p>
                    <br></br>
                    <Link to="/Invite"><button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={itemStyle} data-target="#myModal"><span style={kariaStyle}><b>Daha Fazla Bilgi Al</b></span></button></Link>
                </div>
            </div>

           
        </div>
    </div>

    <div id="testimonials" className="section lb">
        <div className="container">
            <div className="section-title row text-center">
                <div className="col-md-8 col-md-offset-2">
                    <h3>Ev Sahiplerimizin Yorumları</h3>
                    <p className="lead">Ev sahiplerimiz bizim hakkımızda ne diyor?</p>
                </div>
            </div>
            <div className="row" >
                <OwlCarousel
                    {...this.state.options}
                    nav>

                    <div className="item">
                        <div className="testimonial clearfix" >
                            <div className="desc" style={itemStyle} >
                            
                                <h3><i className="fa fa-quote-left s" style={kariaStyle}></i> <span style={kariaStyle}>Profesyonel Destek! </span><i className="fa fa-quote-right" style={kariaStyle}></i></h3>
                                <p className="lead">KariaBNB, profesyonel bir mülk yöneticisi ile çalışmanın iyi ve kolay bir gelir kaynağı olduğunun kanıtı!</p>
                            </div>
                            <div className="testi-meta">
                                
                                <h4>Ali Veli<small>- Ali V.</small></h4>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="testimonial clearfix" >
                            <div className="desc" style={itemStyle} >
                            
                                <h3><i className="fa fa-quote-left s" style={kariaStyle}></i> <span style={kariaStyle}>Profesyonel Destek! </span><i className="fa fa-quote-right" style={kariaStyle}></i></h3>
                                <p className="lead">KariaBNB, profesyonel bir mülk yöneticisi ile çalışmanın iyi ve kolay bir gelir kaynağı olduğunun kanıtı!</p>
                            </div>
                            <div className="testi-meta">
                                
                                <h4>Ali Veli<small>- Ali V.</small></h4>
                            </div>
                        </div>
                    </div>
                    
                </OwlCarousel>
        </div>
    </div>
    
    </div>
    <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
                            <li><a href="/About"> Hakkımızda</a></li>
                            <li><a href="/FAQ"> SSS</a></li>
                            <li><a href="/Invite"> Tavsiye Et Kazan</a></li>
							
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
   
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default MainPage;
