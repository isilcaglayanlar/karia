import React from 'react';
import './App.css';
import OwlCarousel from 'react-owl-carousel';  
import { Link } from 'react-router-dom';
import './css/animate.css';
import './css/bootstrap.min.css';
import './css/bootstrap-theme.min.css';
import './css/bootstrap-theme.css';
import './css/custom.css';
import './css/versions.css';
import './style.css';
import './css/main.css';
import './css/slideshow.css';
import './css/util.css';
import './css/responsive.css';
import './css/flaticon.css';
import './css/owl.carousel.css';


const kariaStyle = {
 color: "#2ccbdd",
};

class AboutPage extends React.Component  {
    //constructor
    constructor(props) {
        super(props);
        this.state = {
            
            options: {
                loop: true,
                margin: 30,
                nav:true,
                
                responsive:{
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 1,
                    },
                    1000: {
                        items: 4,
                    },
                },
            },
        };
    };

 


  render(){
    return (
    <div class="App">

<header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/#"> Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><a className="active" href="/About">Hakkımızda</a></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div className="all-title-box3">
		      <div className="container">
			      <div className="row">
			      	<div className="col-md-12">
				        
					      
				      </div>
			      </div>
		      </div>
	      </div>
 
 <div className="about-box">
		<div className="container">
			<div className="row">
				<div className="top-feature owl-carousel owl-theme">
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-01.png" className="img-responsive" alt=""></img></div>
							<h4>Full Furnished</h4>
							<p>Mauris eu porta orci. In at erat enim. Suspendisse felis erat, volutpat at nisl sit amet, maximus molestie nisi. </p>
						</div> 
					</div>
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-02.png" className="img-responsive" alt=""></img></div>
							<h4>Living Inside a Nature</h4>
							<p>Mauris eu porta orci. In at erat enim. Suspendisse felis erat, volutpat at nisl sit amet, maximus molestie nisi.</p>
						</div> 
					</div>
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-03.png" className="img-responsive" alt=""></img></div>
							<h4>Luxurious Fittings</h4>
							<p>Mauris eu porta orci. In at erat enim. Suspendisse felis erat, volutpat at nisl sit amet, maximus molestie nisi. </p>
						</div> 
					</div>
					<div className="item">
						<div className="single-feature">
							<div className="icon"><img src="uploads/icon-04.png" className="img-responsive" alt=""></img></div>
							<h4>Non Stop Security</h4>
							<p>Lorem Is a dummy Mauris eu porta orci. In at erat enim. Suspendisse felis erat, volutpat at nisl sit amet, maximus molestie nisi. </p>
						</div> 
					</div>
				</div>
			</div>
			
	
			<div className="row">
				<div className="col-md-6">
                    <div className="post-media wow fadeIn" >
                        <img src="uploads/karia.jpg" alt="" className="img-responsive"></img>                   
                    </div>
                </div>
				<div className="col-md-6">
					<div className="message-box right-ab">
                        <br></br>
                        <br></br>
                        <br></br>
                        <h2><span style={kariaStyle}><b>KariaBNB</b></span>'nin anlamı nedir?</h2>
                        <p>İsmini, Ege kıyılarında uygarlığın ve kolonileşmenin öncüsü olan Karialıların yerleştiği Karia bölgesinden alan KariaBNB;
                        geleneksel Türk misafirperverlik ve ilk olarak İngiltere'de manastırların gezginlere konaklama ve yemek sağlaması fikriyle
                        ortaya çıkmış B&B (Bed&Breakfast) kültürünün harmanıdır.

                        </p>
						
                    </div>
				</div>
			</div>
			
		</div>
	</div>

    

   
    <div id="testimonials" className="section lb">
        <div className="container">
            <div className="section-title row text-center">
                <div className="col-md-8 col-md-offset-2">
                    <h3>Ekibimiz</h3>
                    
                </div>
            </div>
            <div className="row" >
                <OwlCarousel  {...this.state.options}
                    nav>


                    <div class="item">
                        <div className="testimonial clearfix" >
                            <div className="desc" style={{backgroundColor:"#ffffff",borderColor:"#ffffff"}} >
                            <img src="uploads/deniz.png" alt="" className="img-responsive aligncenter"></img>
                                <br></br>
                                <h3><span style={kariaStyle}><a href="https://www.linkedin.com/in/deniz-garip-yilmaz-70374848/">Deniz Garip YILMAZ</a></span></h3>
                                <p className="lead">Kurucu</p>
                            </div>
                            <div className="testi-meta">
                                
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div className="testimonial clearfix" >
                            <div className="desc" style={{backgroundColor:"#ffffff",borderColor:"#ffffff"}} >
                            <img src="uploads/kadir.png" alt="" className="img-responsive aligncenter"></img>
                                <br></br>
                                <h3><span style={kariaStyle}><a href="https://www.linkedin.com/in/abdulkadir-kahraman/">Abdülkadir KAHRAMAN</a></span></h3>
                                <p className="lead">Back-end Developer</p>
                            </div>
                            <div className="testi-meta">
                                
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div className="testimonial clearfix" >
                            <div className="desc" style={{backgroundColor:"#ffffff",borderColor:"#ffffff"}} >
                            <img src="uploads/ışıl.png" alt="" className="img-responsive aligncenter"></img>
                                <br></br>
                                <h3><span style={kariaStyle}><a href="https://www.linkedin.com/in/i%C5%9F%C4%B1l-%C3%A7a%C4%9Flayanlar-67a54315a/">Işıl ÇAĞLAYANLAR</a></span></h3>
                                <p className="lead">Front-end Developer</p>
                            </div>
                            <div className="testi-meta">
                                
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div className="testimonial clearfix" >
                            <div className="desc" style={{backgroundColor:"#ffffff",borderColor:"#ffffff"}} >
                            <img src="uploads/süleyman.png" alt="" className="img-responsive aligncenter"></img>
                                <br></br>
                                <h3><span style={kariaStyle}><a href="https://www.linkedin.com/in/suleymantarsuslu/">Süleyman TARSUSLU</a></span></h3>
                                <p className="lead">Front-end Developer</p>
                            </div>
                            <div className="testi-meta">
                                
                            </div>
                        </div>
                    </div>
                    
                </OwlCarousel>
        </div>
    </div>
    
    </div>

	
    <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default AboutPage;