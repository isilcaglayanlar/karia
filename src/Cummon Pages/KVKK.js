import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

class KVKKPage extends React.Component  {
   


  render(){
    return (
    <div className="App">
       <header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/#"> Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
 

	
	
	
    <div className="parallax first-section">
        <div className="container">
            <div className="row">
               
				<div className="col-md-12 col-sm-12 center " style={{backgroundColor:"#e8e8e8"}}>
                    <br></br>
                    <br></br>
                    <ul>
                    <li><h1><b><span style={{color:"#000000"}}> AYDINLATMA METNİ</span></b></h1></li>
                    <br>
                    </br>
                    
                    <li><b>1.</b> Bu beyan KariaBNB gizlilik politikasını ve Kişisel Verilerin Korunmasına ilişkin yasal aydınlatma yükümlülüğünü içerir. KariaBNB, siteyi ziyaret eden kullanıcıların, bireysel ve kurumsal anlamda gizliliğini korumak ve sitede sunulan tüm hizmetlerden en iyi şekilde faydalanmasının sağlanabilmesi amacıyla; bilgi ve veri güvenliği için çeşitli ilkeler benimsemiştir. Bu gizlilik ilkeleri; KariaBNB ve bağlı tüm siteler, mobil uygulamaları ve tüm alt hizmetler üzerinde veri toplanması ve/veya kullanımı konusunda uygulanmak üzere belirlenmiş ve beyan edilmektedir. KariaBNB.com ve mobil uygulamalarını kullanmakla bu ilkelerin tümü kullanıcılar tarafından kabul edilmiş addolunur.</li>
                    <li><b>2.</b> KariaBNB.com üyelik aşamasında ve daha sonrasında sizden bazı kişisel bilgilerinizi talep eder. Kişisel bilgilerinizin korunması ve gizliliğinizin sürdürülebilmesi KariaBNB ekibi olarak birinci önceliğimizdir. Bu nedenle vermiş olduğunuz bilgiler Kullanıcı Sözleşmesi’nde belirtilen kurallar ve amaçlar dışında herhangi bir kapsamda kullanılmayacak, üçüncü şahıslarla paylaşılmayacaktır.</li>
                    <li><b>3.</b> KariaBNB kullanıcılarının güvenliğini ve gizliliğini en ön planda tutulmaktadır. Kullanıcıların sisteme girmiş olduğu bilgiler, gizlilik ve güven içinde, ayrıca bu verilerin kritik olduğunun bilincinde olarak, en güncel ve en gelişmiş güvenlik teknolojileri kullanılarak korunmaktadır. Ayrıca, sektörel gelişmeler ve veri güvenliği alanındaki gelişen tehlikeler, sürekli olarak takip edilerek ve düzenli testler yürütülerek olası saldırılara ve veri hırsızlığı teşebbüslerine karşı en yüksek düzeyde koruma sağlanmaktadır.   </li>
                    <li><b>4.</b> Kullanıcıların, sistemdeki hesaplarına erişim SSL (Secure Socket Layer) teknolojisi kullanılarak üçüncü kişilerden saklanır. SSL teknolojisi, sunucu ile kullanıcının internet tarayıcısı arasında, kimlik doğrulama ve bağlantı şifreleme gibi iki temel görev üstlenir.</li>
                    <li><b>5.</b> KariaBNB, kullanıcının isteği dışında ticari iletişim faaliyeti yapmamayı, izin alınmaksızın pazarlama yapmamayı, kullanıcının sistemden ücretsiz ve kolayca çıkabilmesini sağlayacak araçlar sunmayı beyan ve taahhüt eder.</li>
                    <li><b>6.</b> Sistemle ilgili sorunların tespiti ve söz konusu sorunların en hızlı şekilde giderilebilmesi için, KariaBNB.com, gerektiğinde kullanıcıların IP adresini tespit etmekte ve bunu kullanmaktadır. IP adresleri, kullanıcıları genel bir şekilde tanımlamak ve kapsamlı demografik bilgi toplamak amacıyla da kullanılabilir.</li>
                    <li><b>7.</b> KariaBNB.com’un 5651 sayılı yasada ve bağlı yönetmeliklerde belirtilen trafik verisi saklama yükümlülükleri ile 213 sayılı Vergi Usul Kanunu ve bağlı yönetmeliklerde belirtilen işlem verisi saklama yükümlülükleri ayrıca saklıdır.   </li>
                    <li><b>8.</b> KariaBNB.com, sağladığı hizmet içerisinden ve mobil uygulamalarından başkaca sitelere link (bağlantı) sağlayabilir. Anlaşmalı olduğu 3. Partilerin reklamlarını, başvuru formlarını, duyurularını, anketlerini yayınlayabilir. KariaBNB.com, Kullanıcıları bu formlar, duyurular, anketler ve reklamlar aracılığıyla reklam veren veya anlaşmalı 3. partilerin sitelerine / hizmetlerine / mobil uygulamalarına yönlendirebilir. KariaBNB.com, bu link (bağlantı) yoluyla erişilen diğer sitelerin ve/veya mobil uygulamaların gizlilik uygulama ve politikalarına, ayrıca barındırdıkları görsel, metinsel her tür içeriklerine ilişkin olarak hiç bir sorumluluk taşımamaktadır.</li>
                    <li><b>9.</b> Kullanıcılar, site üzerinden bağlantı sağlanan üçüncü kişi ve kurumlara ait sitelerde bulunan hesaplarının gizlilik ve güvenliğinden kendisi sorumludur. Bu üçüncü tarafların gizlilik politikasından KariaBNB.com sorumlu tutulamaz.</li>
                    <li><b>10.</b> Kullanıcılar, site üzerindeki hesap ve şifrelerinin güvenliğinden ve gizliliğinden kendisi sorumludur. Sitenin kullanımı sırasında veya hizmet alımı sırasında herhangi bir verinin yedeklenmemesinden, zarar görmesinden veya herhangi bir kişisel verinin zarar görmesinden doğacak zararlardan KariaBNB.com sorumlu değildir.</li>
                    <li><b>11.</b> Kullanıcıya ait bireysel / kurumsal bilgiler, ad ve soy ad, adres, telefon numarası, elektronik posta adresi ve kullanıcıyı tanımlamaya yönelik diğer her türlü bilgi olarak anlaşılır. KariaBNB.com, işbu gizlilik bildiriminde aksi belirtilmedikçe bireysel / kurumsal bilgilerden herhangi birini üçüncü kişilere hiç bir şekilde açıklamayacaktır. KariaBNB.com, aşağıda sayılan hallerde ise işbu gizlilik bildirimi hükümleri dışına çıkarak kullanıcılara ait bilgileri üçüncü kişilere açıklayabilecektir. Bu durumlar;</li>
                    <li><b>11.1.</b> Yürürlükte bulunan mevzuat ve iç hukukta da etkili uluslararası anlaşmalardaki yasal zorunluluklara uyulmasının gerektiği haller,</li>
                    <li><b>11.2.</b> KariaBNB.com’un kullanıcılarıyla arasındaki sözleşmelerin gereklerinin yerine getirilmesi ve bunların uygulamaya konulmalarıyla ilgili hallerde,</li>
                    <li><b>11.3.</b> Yetkili idari ve/veya adli makamlar tarafından usuli yöntemine uygun olarak yürütülen bir araştırma veya soruşturma doğrultusunda kullanıcılarla ilgili bilgi talep edilmesi hallerinde,</li>
                    <li><b>11.4.</b> Kullanıcıların haklarını veya güvenliklerini koruma amacıyla bilgi verilmesinin gerekli olduğu hallerde.</li>
                    <li><b>12.</b> KariaBNB.com, kendisine verilen gizli bilgileri kesinlikle özel ve gizli tutmayı, bunu bir sır olarak saklamayı yükümlülük olarak kabul ettiğini ve gizliliğin sağlanıp sürdürülmesi, gizli bilginin tamamının veya herhangi bir parçasının kamu alanına girmesini veya yetkisiz kullanımını veya üçüncü bir kişiye açıklanmasını önleme gereği olan gerekli tüm teknik, hukuki ve yönetimsel tedbirleri almayı ve üzerine düşen tüm özeni tam olarak göstermeyi işbu bildirimle taahhüt etmektedir. </li>
                    <li><b>13.</b> Sitede ücretli olarak sunulan bir hizmetten faydalanılması durumunda veya sitede yayınlanan ilanlar üzerinden satın alma yapıldığı durumlarda, Kullanıcıların, Site üzerinden yaptıkları alışverişler sırasında ödeme kuruluşu veya ödeme aracısı üzerinden toplanan kredi kartı bilgileri, satın alınan ürün ve hizmetlerin faturalandırılması amacıyla kullanılabilmektedir.  </li>
                    <li><b>14.</b> Online olarak kredi kartı ile verilen siparişlerin ödeme/fatura/teslimat adresi bilgilerinin güvenilirliği KariaBNB.com tarafından Kredi Kartları Dolandırıcılığı’na karşı denetlenmektedir. Bu sebeple, ilk defa sipariş veren bazı kullanıcıların öncelikle finansal ve adres/telefon bilgilerinin doğruluğunun onaylanması gerekebilir. Bu bilgilerin kontrolü için gerekirse kredi kartı sahibi Kullanıcı ile veya ilgili banka ile irtibata geçilebilir.</li>
                    <li><b>15.</b> KariaBNB.com göndereceği e-postalarda, hiç bir şekilde kredi kartı numarası, kullanıcı adı, şifre veya parola talep etmez. E-postalarda yer alan veriler üçüncü şahıslar tarafından görülebilir. KariaBNB.com, Kullanıcı e-postalarından aktarılan bilgilerin güvenliğini hiçbir koşulda garanti edemez.</li>
                    <li><b>16.</b> KariaBNB.com, kullanıcıların KariaBNB.com ve mobil uygulamalardaki kullanım bilgilerini, teknik bir iletişim dosyası olan çerezler (Cookie) kullanarak, Uygulama kullanımı hakkındaki bilgileri IP ya da Sosyal Ağ hesabı kullanıcı verileri ile elde edebilir. Çerez denilen teknik iletişim dosyası, internet sitesini zamansal oranlamalı olarak kaç kişinin kullandığını, bir kişinin ilgili internet sitesini hangi amaçla, kaç kez ziyaret ettiği ve ne kadar kaldığı hakkında istatistiki bilgiler elde etmeye ve kullanıcılar için özel olarak tasarlanmış kullanıcı sayfalarından dinamik çeşitlilikle reklam ve içerik üretilmesine yardımcı olmak üzere tasarlanan ve kullanılan veri dosyasıdır. Kullanıcılar dilerlerse çerezlerin bilgisayarlarına yerleştirilmemesi veya bu türden bir dosyasının gönderildiğinde ikaz verilmesini sağlayacak biçimde tarayıcılarının ayarlarını her zaman değiştirebilirler.</li>
                    <li><b>17.</b> KariaBNB.com tarafından site ya da Uygulamalar içerisinde düzenlenebilecek periyodik veya periyodik olmayan anketlere cevap veren, veya yardım masasına (help desk) soru soran kullanıcılardan talep edilebilecek bilgiler de, KariaBNB.com ve işbirliği içindeki kişi ya da kurumlar tarafından bu kullanıcılara doğrudan pazarlama yapmak, istatistiki analizler yapmak ve özel bir veri tabanı oluşturmak amacıyla da kullanılabilmektedir. Anketler veya yardım masası, anlaşma gereği 3. Partiler tarafından sunulan hizmetler olabilir.</li>
                    <li><b>18.</b> 6698 sayılı Kişisel Verilerin İşlenmesi Hakkında Kanun uyarınca, elde edilen kişisel veriler KariaBNB.com tarafından, hukuka ve dürüstlük kuralına uygun, güncel, meşru amaçlar için, ölçülü ve ilgili mevzuata uygun sürelerle işlenebilir.</li>
                    <li><b>19.</b> Elde edilen kişisel veriler, yurtiçi veya yurtdışında KariaBNB.com’un çalıştığı hizmet servis sağlayıcılarının tesisi bulunan başka bir ülkede ilgili mevzuat ve öngörülen güvenlik önlemleri dahilinde veriler aktarılabilir, depolanabilir, işlenebilir.</li>
                    <li><b>20.</b> Kişisel veriler, (i) Kanunlarda açıkça öngörülmesi, (ii) Bir sözleşmenin kurulması veya ifasıyla doğrudan doğruya ilgili olması kaydıyla sözleşmenin taraflarına ait kişisel verilerin işlenmesinin gerekli olması, (iii) Veri sorumlusunun hukuki yükümlülüğünü yerine getirebilmesi için zorunlu olması, (iv) İlgili kişinin kendisi tarafından alenileştirilmiş olması sebeplerinden biri veya birkaçı ile kişisel veri sahibinin rızası alınmaya gerek olmaksızın işlenebilir.</li>
                    </ul> 
                    <br></br>
                </div>
            </div>
        </div>
    </div>
	
	

    

    <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default KVKKPage;