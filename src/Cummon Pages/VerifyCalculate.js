import React from 'react';
import './App.css'; 
import { Link } from 'react-router-dom';
import './css/owl.carousel.css';
import './css/responsive.css';
import './css/animate.css';
import './css/bootstrap.min.css';
import './css/bootstrap-theme.min.css';
import './css/bootstrap-theme.css';
import './css/custom.css';
import './css/versions.css';
import './css/main.css';
import './css/slideshow.css';
import './css/util.css';
import './css/responsive.css';
import './css/flaticon.css';



const kariaButtonStyle = {
    backgroundColor: "#2ccbdd",
   };

const VerifyAccountPage = ({match}) => {


  var requestOptions = {
    method: 'GET',
    redirect: 'follow'
  };
  
  fetch("http://api.kariabnb.com/customers/verify/"+match.params.id+"/", requestOptions)
        .then(response => response.text())
        .then((dataStr) => {
            let str = JSON.parse(dataStr);
            let x = str.payload;
            let y = x.customer;
            let rNumber = y.roomNumber;
            alert("Evinizin potansiyel aylık geliri "+ rNumber*1200 +",00 - "+ rNumber*3890+",00 ₺ aralığındadır.")
        })
        .catch(error => console.log('error', error));


   
    return(

      <div className="App">
       


	
      <ul className='slideshow'>
        <li>
          <span>Summer</span>
            </li>
            <li>
          <span>Summer</span>
            </li>
            <li>
          <span>Summer</span>
            </li>
            <li>
          <span>Summer</span>
        </li>
        
        
      </ul>
      
        <div className="parallax first-section">
            <div className="container">
                <div className="row">
              
            <div className="col-md-12 col-sm-12 center">
                        <br></br>
                        <span style={{color:"#ffffff"}}>E-postanız onaylandı.</span>
                        <br></br>
                        <br></br>
                        <Link to="/"><button type="submit" class="btn btn-light btn-radius btn-brd " style={kariaButtonStyle} data-toggle="modal"   data-target="#centralModalSm">Ana Sayfaya Dön</button></Link>
                    </div>
                </div>
            </div>
        </div>
      
        
        
    
        <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
          </div>
)

    }

export default VerifyAccountPage;