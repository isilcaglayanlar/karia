import React from 'react';
import './App.css';
import { Link } from 'react-router-dom';

const labelStyle = {
    fontsize: "11px",
};

const kariaButtonStyle = {
    backgroundColor: "#2ccbdd",
    color: "#ffffff",
};


class ContactPage extends React.Component  {
    //constructor
    constructor(props) {
        super(props);
        this.state = {
            name:"",
            surname:"",
            email:"",
            countryCode:"",
            phone:"",
            ref:""
        };
    };

    handlechange = async(event) => {
    
    const target = event.target;
    const name = target.name;
    const value = target.value;

    await this.setState({
      [name]: value,
    });  
  };
  
    contact = async (event) => {
    event.preventDefault();
    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
        name: this.state.name,
        surname: this.state.surname,
        email: this.state.email,
        countryCode: "+90",
        phone: this.state.phone,
        ref:this.state.ref
    });
    console.log(raw)
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    alert(requestOptions)
    fetch("http://api.kariabnb.com/contacts/", requestOptions)
      .then((response) => {response.text()})
      .then(result => alert("Sayın "+this.state.name+" "+this.state.surname+", Mailinize gelen onay mailini onaylayınız. Daha sonra kaydınız oluşturulacak ve sizi arayacağız!"))
      .catch(error => alert('error', error));

    };
  render(){
    return (
    <div className="App">
        <header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to= "/#">Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><a className="active" href="/Contact">Teklif Al</a></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
       <div className="all-title-box">
		      <div className="container">
			      <div className="row">
			      	<div className="col-md-12">
				        
					      
				      </div>
			      </div>
		      </div>
	      </div>
	
	<div id="support" className="section wb">
        <div className="container">
            <div className="section-title text-center">
                <h3>Teklif Al</h3>
                <p className="lead">Teklif talep formunu doldurun. Sizi hemen arayalım!</p>
            </div>
            <div className="row">
                <div className="col-md-8 col-md-offset-2">
                    <div className="contact_form" >
                        <div id="message"></div>
                        <form className="row" onSubmit={this.contact} method="post">
                            <fieldset className="row-fluid">
                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="name" required="required" onChange={this.handlechange} id="first_name" className="form-control" placeholder="Ad"></input>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="surname" required="required" onChange={this.handlechange} id="last_name" className="form-control" placeholder="Soyad"></input>
                                </div>
                                
                                
                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" id="email" className="form-control" value={this.state.email} onChange={this.handlechange} required="required" placeholder="Email"></input>
                                </div>
                                
                                
                                <div className="col-lg-2 col-md-6 col-sm-6 col-xs-12">           
                                            <select name="countryCode"  onChange={this.handlechange} className="form-control" id="">
                                            <option value="90" >TR (+90)</option>
                                            <option value="44" >UK (+44)</option>
                                            <option value="1">USA (+1)</option>
                                            <option value="213">Algeria (+213)</option>
                                            <option value="376">Andorra (+376)</option>
                                            <option value="1264">Anguilla (+1264)</option>
                                            <option value="1268">Antigua &amp; Barbuda (+1268)</option>
                                            <option value="54">Argentina (+54)</option>
                                            <option value="374">Armenia (+374)</option>
                                            <option value="297">Aruba (+297)</option>
                                            <option value="61">Australia (+61)</option>
                                            <option value="43">Austria (+43)</option>
                                            <option value="994">Azerbaijan (+994)</option>
                                            <option value="1242">Bahamas (+1242)</option>
                                            <option value="973">Bahrain (+973)</option>
                                            <option  value="880">Bangladesh (+880)</option>
                                            <option  value="1246">Barbados (+1246)</option>
                                            <option  value="375">Belarus (+375)</option>
                                            <option  value="32">Belgium (+32)</option>
                                            <option  value="501">Belize (+501)</option> 
                                            <option value="7">Turkmenistan (+7)</option>
                                            <option  value="993">Turkmenistan (+993)</option>
                                            <option  value="1649">Turks &amp; Caicos Islands (+1649)</option>
                                            <option value="688">Tuvalu (+688)</option>
                                            <option value="256">Uganda (+256)</option>
                                            <option  value="44">UK (+44)</option> 
                                            <option value="380">Ukraine (+380)</option>
                                            <option  value="971">United Arab Emirates (+971)</option>
                                            <option  value="598">Uruguay (+598)</option>
                                            <option value="1">USA (+1)</option>
                                      
                                       
                                    </select>        
                                </div>          

                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12">        
                                    <input className="form-control" name="phone" value={this.state.phone} onChange={this.handlechange} type="tel" placeholder="XXX-XXX-XXXX" id="example-tel-input"></input>
                                </div>
                                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12 center">        
                                    <input className="form-control" name="ref" type="text" value={this.state.ref} onChange={this.handlechange} placeholder="Tavsiye Kodu" id="code"></input>
                                </div>
                                <br></br>
                                <div className="col-lg-12 col-md-6  col-xs-12">
                                    <button type="submit" className="btn btn-light btn-radius btn-brd " data-toggle="modal" style={kariaButtonStyle} data-target="#myModal">Gönder</button>
                                    <div className="checkbox">
                                        <label style={labelStyle}/><input type="checkbox" required="required" value=""/><a href="terms.html"><u>Kullanım şartlarını</u></a> okudum, kabul ediyorum.
                                    </div>
                                    <div className="checkbox">
                                        <label style={labelStyle}>
                                            <input type="checkbox" required="required" value=""/>
                                                Üyelik Sözleşmesi, Gizlilik & Güvenlik Politikası Ve Kişisel Verilerin Korunması Kanunu uyarınca 
                                                    <a href="/Aydinlatma-Metni"><u> Aydınlatma Metni</u></a>
                                                'ni okudum, kabul ediyorum.
                                            
                                        </label>
                                    </div>
                                  
                                </div>
                            </fieldset>
                        </form>
                        <br></br>
                    </div>
                    <br></br>
                </div>
				<br></br>
				
            </div>
            <br></br>
        </div>
        <br></br>
    </div>
    
    
    <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default ContactPage;