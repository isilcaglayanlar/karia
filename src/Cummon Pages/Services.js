import React from 'react';
import './App.css';
import "./css/pricecss/style.css";
import "./css/pricecss/demo.css";
import { Link } from 'react-router-dom';

const kariaButtonStyle = {
    backgroundColor: "#2ccbdd",
};

class ServicesPage extends React.Component  {
    
  render(){
    return (
    <div className="App">
      
    <header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/#">Ana Sayfa</Link></li>
                            <li><a className="active" href="/Services"> Hizmetlerimiz </a></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><Link to="/Login">Giriş Yap</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <main>
  <article>
  

       <section class="pricing-table">
        <div class="container">
            <div class="block-heading">
              <h1>Hizmetlerimiz</h1>
              <p>İhtiyaçlarınıza en uygun paketi seçin!</p>
            </div>
            <div class="row justify-content-md-center">
                
                <div class="col-md-6 col-lg-6">
                    <div class="item">
                       
                        <div class="heading">
                            <h3>Profil Yönetimi</h3>
                        </div>
                        <p>İlanlarınızı biz yönetelim ve %30 daha fazla rezervasyon alın!</p>
                        <p>İlanlarınızı 15+ platformda yayınlayalım! Daha fazla rezervasyon alın ve daha çok kazanın! Misafirlerine operasyon desteği sağlayabilen ev sahipleri için ideal!
 </p>
                        <div class="features">
                            <h4><span class="feature">15+ Platformda İlan Yönetimi</span> </h4>
                            <h4><span class="feature">Panel Üzerinden Takvim Takibi</span></h4>
                            <h4><span class="feature">Misafirlerle İletişim</span> </h4>
                            <h4><span class="feature">Fiyat Optimizasyonu</span> </h4>
                            <h4><span class="feature">Aylık Raporlama</span> </h4>
                        </div>
                        <div class="price">
                            <h4>Rezervasyonda %15</h4>
                        </div>
                        <Link to="/Contact"><button class="btn btn-block btn-outline-primary" onclick= "window.location.href='contact.html' "  style={{kariaButtonStyle}}  type="submit">Hemen Başla!</button></Link>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="item">
                    <div class="ribbon">Size Özel</div>
                        <div class="heading">
                            <h3>Tam Yönetim</h3>
                        </div>
                        <p>Uçtan uca tüm yönetimi bize bırakın ve %60 daha fazla kazanın!
                        </p>
                        <p>İlan yönetiminden, 7/24 misafir ilişkilerine kadar baştan sona herşeyi biz yönetelim. </p>
                        <div class="features">
                            <h4><span class="feature">Profil Yönetimi Paketi</span> </h4>
                            <h4><span class="feature">Temizlik ve Linen Değişimi</span></h4>
                            <h4><span class="feature">24 Saat Check-In</span> </h4>
                            <h4><span class="feature">Misafirlerle İletişim</span> </h4>
                            <h4><span class="feature">Bakım – Onarım</span> </h4>
                        
                        </div>
                        <div class="price">
                            <h4>Rezervasyonda %30</h4>
                        </div>
                        <Link to="/Contact"><button class="btn btn-block btn-outline-primary" onclick= "window.location.href='contact.html' " style={{kariaButtonStyle}} type="submit">Hemen Başla!</button></Link>
                    </div>
                </div>
            </div>
        </div>
    </section>
  
  </article>
 </main>
 <footer className="footer">
        <div className="container">
            <div className="row">
                

                <div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        

                        <ul className="twitter-widget footer-links">
                            <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
							<li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                        </ul>
                    </div>
                </div>
				
				<div className="col-md-4 col-sm-3 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bize Ulaşın</h3>
                        </div>

                        <ul className="footer-links">
                            <li><a href="mailto:#">info@kariabnb.com</a></li>
                            <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                            <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                            
                        </ul>
                    </div>
                </div>
				
                <div className="col-md-4 col-sm-2 col-xs-12">
                    <div className="widget clearfix">
                        <div className="widget-title">
                            <h3>Bizi Takip Edin</h3>
                        </div>
                        <ul className="footer-links">
                            <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                            <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                            <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                        </ul>
                    </div>
                </div>
				
            </div>
        </div>
    </footer>
    <div className="copyrights">
        <div className="container">
            <div className="footer-distributed">
                

                <div className="footer-right">
                    <form method="get" action="/#">
                        <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                        <i className="fa fa-envelope-o"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
   


    <a href="/#" id="scroll-to-top" className="dmtop global-radius"><i className="fa fa-angle-up"></i></a>
    
     
     
     
     
     </div>
    );
  }
}

export default ServicesPage;