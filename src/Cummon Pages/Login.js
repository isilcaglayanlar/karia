import React from 'react';
import './App.css';
import './css/form.css';
import { Link } from 'react-router-dom';



class LoginPage extends React.Component  {
    //constructor
    constructor(props) {
        super(props);
        this.state = {
            city:"",
            district:"",
            roomNumber:"",
            email:"",
            countryCode:"",
            phone:"",
        };
    };

    handlechange = async(event) => {
    
    const target = event.target;
    const name = target.name;
    const value = target.value;

    await this.setState({
      [name]: value,
    });  
  };
  
    contact = async (event) => {
    event.preventDefault();
    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
        city: this.state.city,
        district: this.state.district,
        roomNumber: this.state.roomNumber,
        email: this.state.email,
        countryCode: this.state.countryCode,
        phone: this.state.phone
    });
    
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    
    fetch("http://api.kariabnb.com/customer", requestOptions)
      .then(response => response.text())
      .then(result => alert("Kaydınız oluşturuldu. Sizi arayacağız."))
      .catch(error => alert('error', error));

    };
    

  render(){
    return (
    <div className="App">

<header className="header header_style_01">
            <nav className="megamenu navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="/#"><img src="images/logos/logo.png" alt="logo"></img></a>
                    </div>
                    <div id="navbar" className="navbar-collapse collapse">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/#"> Ana Sayfa</Link></li>
                            <li><Link to="/Services"> Hizmetlerimiz </Link></li>
                            <li><Link to="/Contact">Teklif Al</Link></li>
                            <li><Link to="/About">Hakkımızda</Link></li>
                            <li><Link to="/FAQ">SSS</Link></li>
                            <li><a className="active" href="/Login">Giriş Yap</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

      <br></br>
      <br></br>
      <br></br>
      
      
       <div className="container-fluid ">
            <div className="row">
                <div className="login">
                <div className="col-sm-6 login-section-wrapper">
                <div className="form-group col-lg-12 mb-9 center" style={{backgroundColor:"#ffffff"}}>
                    <div className="col-sm-9 center">
                    
                 
                            <form action="#!">
                            
                                <div className="form-group col-lg-12 mb-3 ">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" className="form-control" placeholder="email@example.com"></input>
                                </div>
                                <div className="form-group col-lg-12 mb-3 " >
                                    <label for="password">Şifre</label>
                                    <input type="password" name="password" id="password" className="form-control" placeholder=" "></input>
                                </div>
                                
                                
                                <div className="col-md-6 col-sm-6 col-xs-12 center">    
                                <button className="btn btn-light btn-radius btn-brd " style={{backgroundColor:"#2ccbdd", color:"#ffffff"}} type="button" value="Giriş Yap">Giriş Yap</button>
                                <p><a href="#!" className="forgot-password-link">Şifremi unuttum</a></p>
                                <br></br>
                                </div>
                                
                            </form>
                            
                            
                            </div>
                    </div>
                </div>
                <div className="col-sm-6 right">
                    <img src="assets/images/login.jpg" alt="login" style={{width:"70%" ,height:"70%"}} className="login-img"></img>
                </div>
            </div>
        </div>
        </div>


        <footer className="footer">
  <div className="container">
      <div className="row">
          

          <div className="col-md-4 col-sm-3 col-xs-12">
              <div className="widget clearfix">
                  

                  <ul className="twitter-widget footer-links">
                  <li><a href="/#"> Ana Sayfa </a></li>
                            <li><a href="/Services"> Hizmetlerimiz </a></li>
                            <li><a href="/Contact"> Teklif Al</a></li>
							<li><a href="/About"> Hakkımızda</a></li>
							<li><a href="/FAQ"> SSS</a></li>
                            <li><a href="/Invite"> Tavsiye Et Kazan</a></li>
                  </ul>
              </div>
          </div>
  
  <div className="col-md-4 col-sm-3 col-xs-12">
              <div className="widget clearfix">
                  <div className="widget-title">
                      <h3>Bize Ulaşın</h3>
                  </div>

                  <ul className="footer-links">
                      <li><a href="mailto:#">info@kariabnb.com</a></li>
                      <li><a href="http://www.kariabnb.com">www.kariabnb.com</a></li>
                      <li>Gülbahçe Caddesi, Teknopark İzmir, A8 Binası, No:1/45/21, Gülbahçe, İzmir 35433, TR</li>
                      <li>+61 3 8376 6284</li>
                  </ul>
              </div>
          </div>
  
          <div className="col-md-4 col-sm-2 col-xs-12">
              <div className="widget clearfix">
                  <div className="widget-title">
                      <h3>Bizi Takip Edin</h3>
                  </div>
                  <ul className="footer-links">
                      <li><a href="/#"><i className="fa fa-linkedin"></i> LinkedIn</a></li>
                      <li><a href="/#"><i className="fa fa-facebook"></i> Facebook</a></li>
                      <li><a href="/#"><i className="fa fa-instagram"></i> Instagram</a></li>
                  </ul>
              </div>
          </div>
  
      </div>
  </div>
</footer>
<div className="copyrights">
  <div className="container">
      <div className="footer-distributed">
          

          <div className="footer-right">
              <form method="get" action="/#">
                  <input placeholder="Gelişmelerden haberdar olun..." name="search"></input>
                  <i className="fa fa-envelope-o"></i>
              </form>
          </div>
      </div>
  </div>
</div>
    </div>
    );
  }
}

export default LoginPage;