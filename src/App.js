import React from 'react';
import './App.css';
import { 
    BrowserRouter as Router, 
    Route,
    Switch,
    Redirect
} from "react-router-dom";

import MainPage from "./Cummon Pages/Body";
import NotFoundPage from "./Cummon Pages/404";
import ServicesPage from "./Cummon Pages/Services";
import AboutPage from "./Cummon Pages/About";
import ContactPage from "./Cummon Pages/Contact";
import FAQPage from "./Cummon Pages/FAQ";
import LoginPage from "./Cummon Pages/Login";
import KVKKPage from "./Cummon Pages/KVKK";
import InvitePage from "./Cummon Pages/Invite";
import InviteTermsPage from "./Cummon Pages/InviteTerms";
import VerifyAccountPage from "./Cummon Pages/VerifyAccount";
import VerifyCalculatePage from "./Cummon Pages/VerifyCalculate";



class App extends React.Component{
    render(){
        let owl_carousel = require('owl.carousel');
window.fn = owl_carousel;

        return(
        <Router>
            <Switch>
                <Route exact path ="/" component={MainPage} />
                <Route exact path ="/404" component={NotFoundPage} />
                <Route exact path ="/Services" component={ServicesPage} />
                <Route exact path ="/About" component={AboutPage} />
                <Route exact path ="/Contact" component={ContactPage} />
                <Route exact path ="/FAQ" component={FAQPage} />
                <Route exact path ="/Login" component={LoginPage} />
                <Route exact path ="/Aydinlatma-Metni" component={KVKKPage} />
                <Route exact path ="/Invite" component={InvitePage} />
                <Route exact path ="/Davet-Sartlari" component={InviteTermsPage} />
                <Route exact path ="/verify-account/:id" component={VerifyAccountPage} />
                <Route exact path ="/verify-and-calculate/:id" component={VerifyCalculatePage} />
                <Redirect to="/404"></Redirect>
            </Switch>
        </Router>
        )
    }

}



export default App;